import React from "react";
import Box from "@mui/material/Box";
import { styled } from "@mui/material/styles";
import { makeStyles } from "@mui/styles";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Services from "../assets/images/Contacts.png";

const CustomTypograpy = styled(Typography)(({ theme }) => ({
  fontWeight: "600",
  color: "#002F52",
  marginBottom: "60px",
  textTransform: "uppercase",
}));
const useStyles = makeStyles({
  myImg: {
    backgroundRepeat: "no-repeat",
    objectFit: "contain",
    borderRadius: 5,
    width: "100%",
    height: "auto",
    marginTop: { lg: "-50px", md: "-30px", lg: "10px" },
  },
});
const Cards = (props) => {
  const classes = useStyles();
  return (
    <Box sx={{ fontWeight: "400", color: "#002F52" }}>
      <Container maxWidth="lg" sx={{ pt: 15 }}>
        <CustomTypograpy component="h1" variant="h4" align="center">
          {props.title}
        </CustomTypograpy>
        <Box>
          <img
            src={Services}
            alt="Sevices"
            width="100%"
            className={classes.myImg}
          />
        </Box>
        <Typography variant="body1" align="justify" sx={{ paddingTop: 1 }}>
          {props.desc}
        </Typography>
      </Container>
    </Box>
  );
};
export default Cards;
