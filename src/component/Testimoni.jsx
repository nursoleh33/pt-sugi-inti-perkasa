import React from "react";
import {
  Card,
  CardMedia,
  Box,
  CardContent,
  Typography,
  CardActionArea,
} from "@mui/material";

function CardTestimoni({ jabatan, description, name, image, testimoni }) {
  return (
    <Box
      sx={{
        position: "relative",
        width: "230px",
        height: "350px",
        marginBottom: { xs: 10, md: 5, lg: 5 },
        // marginLeft: 20,
        // marginRight: "auto",
        textAlign: "left",
      }}
    >
      <CardMedia
        component="img"
        sx={{
          objectFit: "cover",
          zIndex: 1,
          borderRadius: 6,
        }}
        image={image}
        alt={name}
      />
      <Box
        sx={{
          backgroundColor: "#FFF",
          maxHeight: { xs: 160, md: "auto", lg: 140 },
          width: { xs: 240, md: 280, lg: 380 },
          borderRadius: 5,
          top: { xs: 160, md: 70, lg: 110 },
          left: { xs: 50, md: 110, lg: 160 },
          right: { xs: 0, md: 0, lg: 0 },
          position: "absolute",
          zIndex: 3,
        }}
      >
        <CardContent>
          <Typography variant="p" component="h4" gutterBottom={false}>
            {name}
          </Typography>
          <Typography
            variant="body2"
            component="p"
            gutterBottom={true}
            sx={{ color: "#6C6C6C" }}
          >
            {jabatan} | {description}
          </Typography>
          <Typography variant="body2" component="p">
            {testimoni}
          </Typography>
        </CardContent>
      </Box>
    </Box>
  );
}

export default CardTestimoni;
