import React from "react";
import { Card, CardMedia, Box, CardContent, Typography } from "@mui/material";

function Clients({ image, name, description }) {
  return (
    <Card sx={{ display: "flex", paddingLeft: "15px" }}>
      <CardMedia
        component="img"
        sx={{
          width: "130px",
          objectFit: "cover",
          padding: "20px",
        }}
        image={image}
        height="130px"
        alt={name}
      />
      <Box sx={{ display: "flex", flexDirection: "column" }}>
        <CardContent sx={{ flex: "auto" }}>
          <Typography variant="p" component="h4">
            {name}
          </Typography>
          <Typography variant="p" component="p" align="justify">
            {description}
          </Typography>
        </CardContent>
      </Box>
    </Card>
  );
}

export default Clients;
