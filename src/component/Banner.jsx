import React from "react";
import { makeStyles } from "@mui/styles";
import Box from "@mui/material/Box";

const useStyles = makeStyles({
  myBanner: {
    backgroundRepeat: "no-repeat",
    objectFit: "cover",
    // height: "330px",
    marginBottom: "20px",
  },
});

const Banner = ({ image, title }) => {
  const classes = useStyles();
  return (
    <Box>
      <img
        src={image}
        alt={title}
        width="100%"
        height="auto"
        className={classes.myBanner}
      />
    </Box>
  );
};
export default Banner;
