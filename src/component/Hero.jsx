import React from "react";
import { Box, Container } from "@mui/system";
import { Typography, Grid } from "@mui/material";
import {
  createTheme,
  responsiveFontSizes,
  ThemeProvider,
} from "@mui/material/styles";
import Button from "@mui/material/Button";
import "../App.css";

function Jumbotron() {
  let theme = createTheme();
  theme = responsiveFontSizes(theme);
  theme.typography.h3 = {
    fontSize: "1.1rem",
    // color: "yellow",
    "@media (min-width:600px)": {
      fontSize: "1.8rem",
      // color: "blue",
    },
    [theme.breakpoints.up("md")]: {
      fontSize: "2.4rem",
      // color: "red",
    },
  };
  theme.typography.p = {
    fontSize: "0.8rem",
    // color: "yellow",
    "@media (min-width:600px)": {
      fontSize: "1.1rem",
      // color: "blue",
    },
    [theme.breakpoints.up("md")]: {
      fontSize: "1.5rem",
      // color: "red",
    },
  };
  return (
    <Box className="hero">
      <Container
        maxWidth="lg"
        className="headline"
        sx={{ paddingTop: { xs: 15, md: 18, lg: 18 } }}
      >
        <Typography variant="h3">
          We offer a wide range of industrial solutions
        </Typography>
        <Typography variant="h3" gutterBottom={true}>
          for your business needs
        </Typography>
        <Typography variant="p" gutterBottom={true}>
          We offer comprehensive solutions through our extensive range of
          machines and equipment
        </Typography>
        <Typography variant="p">to serve industrial needs.</Typography>
        <Box
          sx={{
            marginTop: 3,
            display: "flex",
            flexWrap: "nowrap",
            justifyContent: "center",
            alignItems: "center",
            gap: 4,
          }}
        >
          <Button
            variant="contained"
            color="primary"
            size="medium"
            href="/service"
          >
            See Our Services
          </Button>
          <Button
            variant="outlined"
            color="white"
            size="medium"
            href="/contact"
          >
            Contact Us
          </Button>
        </Box>
      </Container>
    </Box>
  );
}
export default Jumbotron;
