import React from "react";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { CardActionArea } from "@mui/material";

function CardsProducts({ img, desc, headline, id, name }) {
  const nav = useNavigate();
  const handleClick = (id) => {
    nav(`/productdetail/${id}`);
  };
  return (
    <>
      <Card
        sx={{ width: 280, height: 330 }}
        raised={true}
        onClick={() => handleClick(id)}
      >
        <CardActionArea>
          <CardMedia
            component="img"
            height="100%"
            image={img}
            alt={name}
            style={{ height: 260 }}
          />
          <CardContent>
            <Typography variant="p" component="h3" align="center" gutterBottom>
              {headline}
            </Typography>
            <Typography align="center" component="p">
              {desc}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </>
  );
}

export default CardsProducts;
