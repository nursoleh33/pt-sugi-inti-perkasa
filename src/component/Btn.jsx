import React from "react";
import { Box, Button } from "@mui/material";
import ArrowCircleRightIcon from "@mui/icons-material/ArrowCircleRight";

const Btn = () => {
  return (
    <Box sx={{ marginTop: 5 }}>
      <Button
        variant="contained"
        color="primary"
        size="large"
        to="/contact"
        sx={{
          fontWeight: "bold",
        }}
      >
        Contact Us
        <ArrowCircleRightIcon
          color="#FFF"
          fontSize="large"
          sx={{ paddingLeft: 2 }}
        />
      </Button>
    </Box>
  );
};
export default Btn;
