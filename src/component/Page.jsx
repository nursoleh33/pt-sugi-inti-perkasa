import React from "react";
import "../App.css";

const Page = ({
  page,
  handleNext,
  handlePrevius,
  totalpage,
  prevState,
  nextState,
}) => {
  return (
    <>
      <div className="container">
        <button
          className={prevState === false ? `disable` : "previus"}
          onClick={prevState === false ? () => {} : handlePrevius}
        >
          Previus
        </button>
        <button className="page">{page}</button>
        <button
          className={nextState === false ? `hidden` : "next"}
          onClick={nextState === false ? () => {} : handleNext}
        >
          Next
        </button>
      </div>
    </>
  );
};

export default Page;
