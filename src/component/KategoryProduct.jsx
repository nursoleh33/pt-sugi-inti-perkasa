import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
} from "@mui/material";

function KategoryProduct({ id, name, image, file }) {
  const nav = useNavigate();
  const handleProducts = (id, file) => {
    // console.log(id);
    // console.log(file);
    if (id) {
      nav(`/categori/${id}`);
    } else {
      window.open(file, "_blank");
    }
  };
  return (
    <div>
      <Card
        sx={{
          maxWidth: 240,
          marginTop: 10,
          maxHeight: 330,
        }}
        raised={true}
        onClick={() => handleProducts(id, file)}
      >
        <CardActionArea>
          <CardMedia component="img" height="100%" image={image} alt={name} />
          <CardContent>
            <Typography variant="h6" align="center">
              {name}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </div>
  );
}
export default KategoryProduct;
