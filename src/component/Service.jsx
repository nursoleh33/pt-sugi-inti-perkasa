import React from "react";
import { Box, CardContent, Typography, CardMedia, Grid } from "@mui/material";
import { makeStyles } from "@mui/styles";
import Btn from "./Btn";
const useStyles = makeStyles({
  myCard: {
    display: "flex",
    width: "auto",
    padding: 8,
    marginBottom: 15,
    marginLeft: 20,
  },
});

export default function CardService({ image, title, desc }) {
  return (
    <Grid container alignItems="start" justifyContent="start">
      <Grid
        item
        xs={12}
        lg={4}
        md={4}
        sx={{ order: { xs: 1, md: 1 }, marginTop: 10 }}
      >
        <CardMedia
          component="img"
          sx={{
            width: "100%",
            objectFit: "contain",
          }}
          image={image}
          height="250px"
          alt={title}
        />
      </Grid>
      <Grid
        item
        xs={12}
        lg={7}
        md={7}
        sx={{ order: { xs: 2, md: 2 }, marginTop: { xs: 1, lg: 7 } }}
      >
        <CardContent>
          <Typography
            variant="p"
            component="h2"
            gutterBottom={true}
            sx={{ color: "#333333" }}
          >
            {title}
          </Typography>
          <Typography
            variant="p"
            component="p"
            align="justify"
            sx={{ color: "#595959" }}
          >
            {desc}
          </Typography>
          <Box sx={{ marginTop: 3 }}>
            <Btn />
          </Box>
        </CardContent>
      </Grid>
    </Grid>
    // <Card className={classes.myCard}>
    //   <Grid item xs={12} lg={4} md={4}>
    //     <img src={image} alt={title} width="90%" height="auto" />
    //   </Grid>
    //   <Grid item xs={12} md={8} lg={8}>
    //     <Typography variant="h5" gutterBottom>
    //       {title}
    //     </Typography>
    //     <Typography variant="body1" gutterBottom align="justify">
    //       {desc}
    //     </Typography>
    //     <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
    //       <Button
    //         variant="contained"
    //         color="primary"
    //         size="large"
    //         href="/contact"
    //         sx={{ fontWeight: "bold" }}
    //       >
    //         Contact Us
    //       </Button>
    //     </Box>
    //   </Grid>
    // </Card>
  );
}
