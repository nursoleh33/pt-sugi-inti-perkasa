import React, { useEffect } from "react";
import { useLocation } from "react-router-dom";
import Box from "@mui/system/Box";
import Logo from "../assets/images/logo.png";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { Link } from "react-router-dom";
import { Button, useMediaQuery } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { grey } from "@mui/material/colors";
import "../App.css";
import DrawerComp from "./DrawerComp";
import { useTheme } from "@mui/material";
import { useNavigate } from "react-router-dom";
import "../App.css";

const useStyles = makeStyles({
  myHeadline: {
    color: grey[900],
    fontWeight: "bold",
    paddingLeft: 30,
    cursor: "pointer",
  },
  myLogo: {
    cursor: "pointer",
  },
});
const myText = {
  color: grey[900],
  textDecoration: "none",
  textTransform: "uppercase",
  paddingRight: 25,
  fontWeight: "lighter",
};
const Navbar = () => {
  const classes = useStyles();
  let { pathname } = useLocation();
  const nav = useNavigate();
  const handleContact = () => {
    nav(`/contact`);
  };
  const handleHome = () => {
    nav("/");
  };
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  const theme = useTheme();
  let isMatch = useMediaQuery(theme.breakpoints.down("md"));
  const routes = [
    {
      path: "/",
      name: "Home",
    },
    {
      path: "/product",
      name: "Product",
    },
    {
      path: "/service",
      name: "Service",
    },
    {
      path: "/catalogue",
      name: "Catalog",
    },
    {
      path: "/about",
      name: "About",
    },
  ];
  return (
    <Box>
      <AppBar
        sx={{
          padding: "20px 25px",
          position: "fixed",
          width: "100%",
          backgroundColor: "common.white",
        }}
      >
        <Toolbar>
          {isMatch ? (
            <>
              <Grid container alignItems="center" columnGap={2}>
                <Grid item xs={1} sx={{ cursor: "pointer" }}>
                  <img
                    src={Logo}
                    alt="PT SUGI INTI PERKASA"
                    width="50px"
                    height="50px"
                    className={classes.myLogo}
                    onClick={handleHome}
                  />
                </Grid>
                <Grid item xs>
                  <Typography
                    component="p"
                    variant="h6"
                    align="center"
                    className={classes.myHeadline}
                    onClick={handleHome}
                  >
                    PT. SUGI INTI PERKASA
                  </Typography>
                </Grid>
              </Grid>
              <DrawerComp />
            </>
          ) : (
            <Grid container alignItems="center" spacing={1}>
              <Grid item md="auto" lg="auto">
                <img
                  src={Logo}
                  alt="SUGI"
                  width="50px"
                  height="50px"
                  className={classes.myLogo}
                  onClick={handleHome}
                />
              </Grid>
              <Grid item md="auto" lg="auto">
                <Typography
                  component="h1"
                  variant="h6"
                  className={classes.myHeadline}
                  onClick={handleHome}
                >
                  PT. SUGI INTI PERKASA
                </Typography>
              </Grid>
              <Grid item md lg />
              <Grid
                item
                md="auto"
                lg="auto"
                sx={{ width: "500px", bg: "yellow" }}
              >
                {routes.map((route, index) => {
                  return (
                    <span key={index}>
                      <Link
                        to={route.path}
                        // style={myText}
                        className={`link ${
                          route.path === pathname ? "font" : ""
                        }`}
                      >
                        {route.name}
                      </Link>
                    </span>
                  );
                })}
              </Grid>
              <Grid item md="auto" lg="auto">
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleContact}
                  sx={{ borderRadius: "5px" }}
                >
                  Contact Us
                </Button>
              </Grid>
            </Grid>
          )}
        </Toolbar>
      </AppBar>
    </Box>
  );
};
export default Navbar;
