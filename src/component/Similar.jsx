import React from "react";
import {
  Card,
  Grid,
  CardMedia,
  CardActionArea,
  CardContent,
  Typography,
} from "@mui/material";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/swiper.min.css";
import "../App.css";
import SwiperCore, { Navigation } from "swiper/core";
SwiperCore.use([Navigation]);

function Similar({ img, desc, headline, id }) {
  const handleSubmit = (id) => {
    window.location.href = `/productdetail/${id}`;
    window.scrollTo(0, 0);
  };
  return (
    <Grid item xs="auto" lg={3} md="auto">
      <Card
        sx={{ width: 280, marginTop: 4, height: 390, marginBottom: 5 }}
        raised={true}
        onClick={() => handleSubmit(id)}
      >
        <CardActionArea>
          <CardMedia
            component="img"
            height="100%"
            image={img}
            alt="Cutting Tools"
            style={{ height: 260 }}
          />
          <CardContent>
            <Typography variant="p" component="h3" align="center" gutterBottom>
              {headline}
            </Typography>
            <Typography align="center" variant="p">
              {desc}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  );
}

export default Similar;
