import React from "react";
import { List, ListItem, ListItemText, ListItemIcon } from "@mui/material";
import FiberManualRecordSharpIcon from "@mui/icons-material/FiberManualRecordSharp";

export default function Appliction({ desc }) {
  return (
    <>
      <List>
        <ListItem sx={{ marginTop: "5px" }}>
          <ListItemIcon>
            <FiberManualRecordSharpIcon />
          </ListItemIcon>
          <ListItemText primary={desc} sx={{ marginLeft: "-30px" }} />
        </ListItem>
      </List>
    </>
  );
}
