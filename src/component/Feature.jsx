import React from "react";
import { List, ListItemIcon, ListItemText, ListItem } from "@mui/material";
import FiberManualRecordSharpIcon from "@mui/icons-material/FiberManualRecordSharp";
export default function Feature({ desc }) {
  return (
    <>
      <List>
        <ListItem>
          <ListItemIcon>
            <FiberManualRecordSharpIcon color="primary" fontSize="small" />
          </ListItemIcon>
          <ListItemText
            primary={desc}
            sx={{
              marginLeft: "-30px",
              color: "primary.main",
            }}
          />
        </ListItem>
      </List>
    </>
  );
}
