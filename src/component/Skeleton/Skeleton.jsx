import React from "react";
import Skeleton from "@mui/material/Skeleton";
import { Box, CardContent, Grid, Card, useMediaQuery } from "@mui/material";
import { useTheme } from "@mui/material";
import { makeStyles } from "@mui/styles";
const useStyles = makeStyles({
  cardImage: {
    height: 980,
    width: 180,
  },
  cardContent: {
    height: 30,
    width: 450,
  },
});

function SkeletonClients() {
  const theme = useTheme({});
  let isMatch = useMediaQuery(theme.breakpoints.down("sm"));

  // let isTablet = useMediaQuery(theme.breakpoints.up(""));
  const classes = useStyles();
  return (
    <>
      <Grid item xs={12} lg={12} md={12}>
        {isMatch ? (
          <>
            <Card
              sx={{
                display: "flex",
                paddingLeft: "15px",
                maxWidth: "auto",
                marginBottom: 10,
              }}
              raised={true}
            >
              <Skeleton
                animation="pulse"
                variant="rounded"
                height={100}
                // className={classes.cardImage}
                width={100}
                style={{ marginTop: 20, marginBottom: 20 }}
              />
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                }}
              >
                <CardContent>
                  <Skeleton
                    animation="pulse"
                    height={20}
                    width={250}
                    // className={classes.cardContent}
                    variant="text"
                  />
                  <Skeleton
                    animation="pulse"
                    // className={classes.cardContent}
                    height={20}
                    width={250}
                    variant="text"
                  />
                </CardContent>
              </Box>
            </Card>
            <Card
              sx={{
                display: "flex",
                paddingLeft: "15px",
                maxWidth: "auto",
                marginBottom: 10,
              }}
              raised={true}
            >
              <Skeleton
                animation="pulse"
                variant="rounded"
                height={100}
                // className={classes.cardImage}
                width={100}
                style={{ marginTop: 20, marginBottom: 20 }}
              />
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                }}
              >
                <CardContent>
                  <Skeleton
                    animation="pulse"
                    height={20}
                    width={250}
                    // className={classes.cardContent}
                    variant="text"
                  />
                  <Skeleton
                    animation="pulse"
                    // className={classes.cardContent}
                    height={20}
                    width={250}
                    variant="text"
                  />
                </CardContent>
              </Box>
            </Card>
          </>
        ) : (
          <>
            <Card
              sx={{ display: "flex", paddingLeft: "15px", marginBottom: 10 }}
              raised={true}
            >
              <Skeleton
                animation="pulse"
                variant="rounded"
                height={150}
                width={150}
                // className={classes.cardImage}
                style={{ marginTop: 20, marginBottom: 20 }}
              />
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                }}
              >
                <CardContent>
                  <Skeleton
                    animation="pulse"
                    height={25}
                    width={540}
                    variant="text"
                  />
                  <Skeleton
                    animation="pulse"
                    height={25}
                    width={540}
                    variant="text"
                  />
                </CardContent>
              </Box>
            </Card>
            <Card
              sx={{ display: "flex", paddingLeft: "15px", marginBottom: 10 }}
              raised={true}
            >
              <Skeleton
                animation="pulse"
                variant="rounded"
                height={150}
                width={150}
                // className={classes.cardImage}
                style={{ marginTop: 20, marginBottom: 20 }}
              />
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                }}
              >
                <CardContent>
                  <Skeleton
                    animation="pulse"
                    height={25}
                    width={540}
                    variant="text"
                  />
                  <Skeleton
                    animation="pulse"
                    height={25}
                    width={540}
                    variant="text"
                  />
                </CardContent>
              </Box>
            </Card>
          </>
        )}
      </Grid>
    </>
  );
}

export default SkeletonClients;
