import React from "react";
import Skeleton from "@mui/material/Skeleton";
import { Card, Grid, CardActionArea, CardContent } from "@mui/material";

function SkeletonProducts() {
  return (
    <>
      <Grid item xs="auto" lg={3} md="auto">
        <Card sx={{ width: 280, marginTop: 4, height: 330 }} raised={true}>
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={260}
              width={260}
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: 8,
              }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={20}
                width={180}
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                variant="text"
              />
              <Skeleton
                animation="pulse"
                height={20}
                width={190}
                variant="text"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card sx={{ width: 280, marginTop: 4, height: 330 }} raised={true}>
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={260}
              width={260}
              // count={2}
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: 8,
              }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={20}
                width={180}
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                variant="text"
              />
              <Skeleton
                animation="pulse"
                height={20}
                width={190}
                variant="text"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card sx={{ width: 280, marginTop: 4, height: 330 }} raised={true}>
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={260}
              width={260}
              // count={2}
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: 8,
              }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={20}
                width={180}
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                variant="text"
              />
              <Skeleton
                animation="pulse"
                height={20}
                width={190}
                variant="text"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card sx={{ width: 280, marginTop: 4, height: 330 }} raised={true}>
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={260}
              width={260}
              // count={2}
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: 8,
              }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={20}
                width={180}
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                variant="text"
              />
              <Skeleton
                animation="pulse"
                height={20}
                width={190}
                variant="text"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card sx={{ width: 280, marginTop: 4, height: 330 }} raised={true}>
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={260}
              width={260}
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: 8,
              }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={20}
                width={180}
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                variant="text"
              />
              <Skeleton
                animation="pulse"
                height={20}
                width={190}
                variant="text"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card sx={{ width: 280, marginTop: 4, height: 330 }} raised={true}>
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={260}
              width={260}
              // count={2}
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: 8,
              }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={20}
                width={180}
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                variant="text"
              />
              <Skeleton
                animation="pulse"
                height={20}
                width={190}
                variant="text"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card sx={{ width: 280, marginTop: 4, height: 330 }} raised={true}>
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={260}
              width={260}
              // count={2}
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: 8,
              }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={20}
                width={180}
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                variant="text"
              />
              <Skeleton
                animation="pulse"
                height={20}
                width={190}
                variant="text"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card sx={{ width: 280, marginTop: 4, height: 330 }} raised={true}>
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={260}
              width={260}
              // count={2}
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: 8,
              }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={20}
                width={180}
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                variant="text"
              />
              <Skeleton
                animation="pulse"
                height={20}
                width={190}
                variant="text"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card sx={{ width: 280, marginTop: 4, height: 330 }} raised={true}>
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={260}
              width={260}
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: 8,
              }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={20}
                width={180}
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                variant="text"
              />
              <Skeleton
                animation="pulse"
                height={20}
                width={190}
                variant="text"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card sx={{ width: 280, marginTop: 4, height: 330 }} raised={true}>
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={260}
              width={260}
              // count={2}
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: 8,
              }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={20}
                width={180}
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                variant="text"
              />
              <Skeleton
                animation="pulse"
                height={20}
                width={190}
                variant="text"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card sx={{ width: 280, marginTop: 4, height: 330 }} raised={true}>
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={260}
              width={260}
              // count={2}
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: 8,
              }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={20}
                width={180}
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                variant="text"
              />
              <Skeleton
                animation="pulse"
                height={20}
                width={190}
                variant="text"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card sx={{ width: 280, marginTop: 4, height: 330 }} raised={true}>
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={260}
              width={260}
              // count={2}
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: 8,
              }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={20}
                width={180}
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                variant="text"
              />
              <Skeleton
                animation="pulse"
                height={20}
                width={190}
                variant="text"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
    </>
  );
}

export default SkeletonProducts;
