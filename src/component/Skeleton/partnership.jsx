import React from "react";
import { Grid, Skeleton } from "@mui/material";

function Partnership() {
  return (
    <>
      <Grid item xs={6} lg={3} md={4}>
        <Skeleton animation="wave" variant="rounded" height={118} width={340} />
      </Grid>
      <Grid item xs={6} lg={3} md={4}>
        <Skeleton animation="wave" variant="rounded" height={118} width={340} />
      </Grid>
      <Grid item xs={6} lg={3} md={4}>
        <Skeleton animation="wave" variant="rounded" height={118} width={340} />
      </Grid>
      <Grid item xs={6} lg={3} md={4}>
        <Skeleton animation="wave" variant="rounded" height={118} width={340} />
      </Grid>
      <Grid item xs={6} lg={3} md={4}>
        <Skeleton animation="wave" variant="rounded" height={118} width={340} />
      </Grid>
      <Grid item xs={6} lg={3} md={4}>
        <Skeleton animation="wave" variant="rounded" height={118} width={340} />
      </Grid>
      <Grid item xs={6} lg={3} md={4}>
        <Skeleton animation="wave" variant="rounded" height={118} width={340} />
      </Grid>
      <Grid item xs={6} lg={3} md={4}>
        <Skeleton animation="wave" variant="rounded" height={118} width={340} />
      </Grid>
      <Grid item xs={6} lg={3} md={4}>
        <Skeleton animation="wave" variant="rounded" height={118} width={340} />
      </Grid>
    </>
  );
}

export default Partnership;
