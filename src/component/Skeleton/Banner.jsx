import React from "react";
import { Box, Skeleton } from "@mui/material";

function SkeletonBanner() {
  return (
    <Box style={{ display: "flex", justifyContent: "center" }}>
      <Skeleton
        animation={false}
        variant="rectangular"
        style={{ width: "97vw", marginBottom: 30 }}
        height={330}
      />
    </Box>
  );
}

export default SkeletonBanner;
