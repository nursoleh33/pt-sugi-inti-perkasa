import React from "react";
import { Box, Card, Grid, Skeleton } from "@mui/material";
import { makeStyles } from "@mui/styles";
const useStyles = makeStyles({
  myCard: {
    display: "flex",
    width: "auto",
    padding: 2,
    marginBottom: 15,
    marginLeft: 20,
  },
});

export default function SkeletonService() {
  const classes = useStyles();
  return (
    <>
      <Card className={classes.myCard}>
        <Grid item xs={4} lg={4} md={4}>
          <Skeleton
            animation="pulse"
            variant="rounded"
            height={180}
            width={230}
          />
        </Grid>
        <Grid item xs={8} md={8} lg={8} sx={{ marginLeft: 5 }}>
          <Skeleton
            animation="wave"
            height={25}
            width={340}
            variant="text"
            sx={{ mx: "auto" }}
          />
          <Skeleton animation="wave" height={25} width={410} variant="text" />
          <Skeleton animation="wave" height={25} width={410} variant="text" />
          <Skeleton animation="wave" height={25} width={410} variant="text" />
          <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
            <Skeleton
              animation={false}
              height={40}
              width={60}
              variant="text"
              sx={{ borderRadius: 2 }}
            />
          </Box>
        </Grid>
      </Card>
      <Card className={classes.myCard}>
        <Grid item xs={4} lg={4} md={4}>
          <Skeleton
            animation="pulse"
            variant="rounded"
            height={180}
            width={230}
          />
        </Grid>
        <Grid item xs={8} md={8} lg={8} sx={{ marginLeft: 5 }}>
          <Skeleton
            animation="wave"
            height={25}
            width={340}
            variant="text"
            sx={{ mx: "auto" }}
          />
          <Skeleton animation="wave" height={25} width={410} variant="text" />
          <Skeleton animation="wave" height={25} width={410} variant="text" />
          <Skeleton animation="wave" height={25} width={410} variant="text" />
          <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
            <Skeleton
              animation={false}
              height={40}
              width={60}
              variant="text"
              sx={{ borderRadius: 2 }}
            />
          </Box>
        </Grid>
      </Card>
      <Card className={classes.myCard}>
        <Grid item xs={4} lg={4} md={4}>
          <Skeleton
            animation="pulse"
            variant="rounded"
            height={180}
            width={230}
          />
        </Grid>
        <Grid item xs={8} md={8} lg={8} sx={{ marginLeft: 5 }}>
          <Skeleton
            animation="wave"
            height={25}
            width={340}
            variant="text"
            sx={{ mx: "auto" }}
          />
          <Skeleton animation="wave" height={25} width={410} variant="text" />
          <Skeleton animation="wave" height={25} width={410} variant="text" />
          <Skeleton animation="wave" height={25} width={410} variant="text" />
          <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
            <Skeleton
              animation={false}
              height={40}
              width={60}
              variant="text"
              sx={{ borderRadius: 2 }}
            />
          </Box>
        </Grid>
      </Card>
      <Card className={classes.myCard}>
        <Grid item xs={4} lg={4} md={4}>
          <Skeleton
            animation="pulse"
            variant="rounded"
            height={180}
            width={230}
          />
        </Grid>
        <Grid item xs={8} md={8} lg={8} sx={{ marginLeft: 5 }}>
          <Skeleton
            animation="wave"
            height={25}
            width={340}
            variant="text"
            sx={{ mx: "auto" }}
          />
          <Skeleton animation="wave" height={25} width={410} variant="text" />
          <Skeleton animation="wave" height={25} width={410} variant="text" />
          <Skeleton animation="wave" height={25} width={410} variant="text" />
          <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
            <Skeleton
              animation={false}
              height={40}
              width={60}
              variant="text"
              sx={{ borderRadius: 2 }}
            />
          </Box>
        </Grid>
      </Card>
    </>
  );
}
