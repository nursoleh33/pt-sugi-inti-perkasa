import React from "react";
import { Box, CardContent, Skeleton } from "@mui/material";
import { grey, red } from "@mui/material/colors";
import { makeStyles, styled } from "@mui/styles";
const useStyles = makeStyles({
  myLink: {
    color: red[900],
    fontWeight: "bold",
    textDecoration: "none",
  },
  myData: {
    textDecoration: "none",
    color: grey[900],
  },
  myMaintenace: {
    display: "flex",
    justifyContent: "center",
    color: red,
    alignItems: "center",
    marginTop: 180,
  },
  myNotFound: {
    display: "flex",
    justifyContent: "center",
    fontWeight: "bold",
    color: red,
    alignItems: "center",
    marginTop: 150,
  },
});

export default function SkeletonTestimoni() {
  const classes = useStyles();
  return (
    <>
      <Box
        sx={{
          // position: "relative",
          // width: { lg: 230 },
          // height: { lg: 250 },
          // marginBottom: 5,
          position: "relative",
          width: "230px",
          height: "250px",
          marginBottom: { xs: 10, md: 5, lg: 5 },
          marginLeft: "auto",
          marginRight: "auto",
        }}
      >
        <Skeleton
          animation="pulse"
          variant="rounded"
          height={260}
          width={260}
          style={{
            marginLeft: "auto",
            marginRight: "auto",
            marginTop: 8,
            borderRadius: 10,
          }}
        />
        <Box
          sx={{
            // backgroundColor: { xs: "#6e8b3d", md: "#ab2830", lg: "yellow" },
            maxHeight: 140,
            width: 280,
            borderRadius: 5,
            top: { xs: 170, md: 70, lg: 10 },
            left: { xs: -30, md: 140, lg: 40 },
            position: "absolute",
            zIndex: 3,
          }}
        >
          <CardContent sx={{ flex: "auto" }}>
            <Skeleton
              animation="wave"
              variant="rounded"
              height={130}
              width={290}
              sx={{
                borderRadius: 5,
                top: { lg: 70 },
                left: { lg: 140 },
                position: "absolute",
                zIndex: 3,
              }}
            />
          </CardContent>
        </Box>
      </Box>
    </>
  );
}
