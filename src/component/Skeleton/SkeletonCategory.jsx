import React from "react";
import { Grid, CardActionArea, Card, CardContent } from "@mui/material";
import { Skeleton } from "@mui/material";

function SkeletonKategori() {
  return (
    <>
      <Grid item xs="auto" lg={3} md="auto">
        <Card
          sx={{
            maxWidth: 240,
            marginTop: 4,
            height: 330,
          }}
          raised={true}
        >
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={190}
              width={210}
              style={{ margin: 15 }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={30}
                width={210}
                variant="text"
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card
          sx={{
            maxWidth: 240,
            marginTop: 4,
            height: 330,
          }}
          raised={true}
        >
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={190}
              width={210}
              style={{ margin: 15 }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={30}
                width={210}
                variant="text"
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card
          sx={{
            maxWidth: 240,
            marginTop: 4,
            height: 330,
          }}
          raised={true}
        >
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={190}
              width={210}
              style={{ margin: 15 }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={30}
                width={210}
                variant="text"
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card
          sx={{
            maxWidth: 240,
            marginTop: 4,
            height: 330,
          }}
          raised={true}
        >
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={190}
              width={210}
              style={{ margin: 15 }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={30}
                width={210}
                variant="text"
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card
          sx={{
            maxWidth: 240,
            marginTop: 4,
            height: 330,
          }}
          raised={true}
        >
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={190}
              width={210}
              style={{ margin: 15 }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={30}
                width={210}
                variant="text"
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card
          sx={{
            maxWidth: 240,
            marginTop: 4,
            height: 330,
          }}
          raised={true}
        >
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={190}
              width={210}
              style={{ margin: 15 }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={30}
                width={210}
                variant="text"
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card
          sx={{
            maxWidth: 240,
            marginTop: 4,
            height: 330,
          }}
          raised={true}
        >
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={190}
              width={210}
              style={{ margin: 15 }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={30}
                width={210}
                variant="text"
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs="auto" lg={3} md="auto">
        <Card
          sx={{
            maxWidth: 240,
            marginTop: 4,
            height: 330,
          }}
          raised={true}
        >
          <CardActionArea>
            <Skeleton
              animation="pulse"
              variant="rounded"
              height={190}
              width={210}
              style={{ margin: 15 }}
            />
            <CardContent>
              <Skeleton
                animation="pulse"
                height={30}
                width={210}
                variant="text"
              />
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
    </>
  );
}

export default SkeletonKategori;
