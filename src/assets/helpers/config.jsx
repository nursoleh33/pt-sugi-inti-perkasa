export const publicHeader = () => {
  return {
    // "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json",
  };
};

export const url = {
  /** dengan localhost dan port jika sedang menjalankan di local */
  api: process.env.REACT_APP_API_URL,
};
