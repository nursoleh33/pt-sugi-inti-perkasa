import React, { useState, useEffect } from "react";
import { Box, Container, Typography, Grid } from "@mui/material";
import axios from "axios";
import Catalogue from "../assets/images/catalogue.png";
import { Helmet } from "react-helmet";
import Swal from "sweetalert2";
import { red } from "@mui/material/colors";
import SkeletonKategori from "../component/Skeleton/SkeletonCategory";
import Page from "../component/Page";
import { makeStyles, styled } from "@mui/styles";
import KategoryProduct from "../component/KategoryProduct";
import { url, publicHeader } from "../assets/helpers/config.jsx";

const CustomTypograpy = styled(Typography)(({ theme }) => ({
  fontWeight: "bold",
  color: "#002F52",
  textTransform: "uppercase",
}));
const useStyles = makeStyles({
  myMaintenace: {
    display: "flex",
    justifyContent: "center",
    color: red,
    alignItems: "center",
    marginTop: 180,
  },
  myNotFound: {
    display: "flex",
    justifyContent: "center",
    fontWeight: "bold",
    color: red,
    alignItems: "center",
    marginTop: 150,
  },
});

export default function Catalog() {
  let [catalogue, setCatalogue] = useState([]);
  let [totalpage, setTotalPage] = useState(null);
  let [page, setPage] = useState(null);
  let [previus, setPrevius] = useState(null);
  let [next, setNext] = useState(null);
  let [loading, setLoading] = useState(false);
  let [errors, setErrors] = useState(false);
  let [stock, setStock] = useState(false);
  const getCatalogue = async () => {
    setLoading(false);
    await axios({
      url: `${url.api}/katalog`,
      method: "GET",
      headers: publicHeader(),
    })
      .then((res) => {
        if (res.status === 200) {
          let datacatalogue = res.data.data.data;
          setCatalogue(datacatalogue);
          setPage(
            datacatalogue.length < 12 ? 1 : res?.data?.data?.current_page
          );
          setTotalPage(res?.data?.data?.total);
          setNext(res?.data?.data?.next_page_url == null ? false : true);
          setPrevius(res?.data?.data?.prev_page_url == null ? false : true);
          setLoading(false);

          setErrors(false);
          setStock(!datacatalogue.length === true ? 0 : datacatalogue.length);
        }
        if (res.status === 404 || res.status === 500) {
          setErrors(true);
          setLoading(false);
        }
      })
      .catch((err) => {
        Swal.fire("Sorry", "Data Gagal Di Tampilkan", "warning");
        setErrors(true);
        setLoading(false);
      });
  };
  useEffect(() => {
    getCatalogue();
  }, [page]);
  const classes = useStyles();
  const handleNext = () => {
    if (!loading && totalpage !== null) {
      setPage(page + 1);
    }
  };
  const handlePrevius = () => {
    if (page > 1 && !loading) {
      setPage(page - 1);
    }
  };
  if (errors) {
    return (
      <h1 className={classes.myMaintenace}>Sedang Dalam Maintenace Server</h1>
    );
  }
  if (stock === 0) {
    return <h1 className={classes.myNotFound}>Data Tidak Di Temukan</h1>;
  }
  return (
    <Box sx={{ marginBottom: 10 }}>
      <Helmet>
        <title>Katalog</title>
        <meta name="description" content="Sugi Inti Perkasa" />
      </Helmet>
      <Container maxWidth="lg" sx={{ paddingTop: "110px" }}>
        <CustomTypograpy variant="h4" component="h1" align="center">
          CATALOGUE
        </CustomTypograpy>
        <Grid
          container
          alignItems="center"
          spacing={6}
          sx={{ display: "flex", justifyContent: "center", marginBottom: 5 }}
        >
          {loading ? (
            <>
              <SkeletonKategori />
            </>
          ) : (
            catalogue.map((limitproduct) => {
              return (
                <Grid item xs="auto" lg={3} md="auto" key={limitproduct.id}>
                  <>
                    <KategoryProduct
                      image={Catalogue}
                      name={limitproduct.name}
                      file={limitproduct.file_url}
                    />
                  </>
                </Grid>
              );
            })
          )}
        </Grid>
      </Container>
      <Page
        page={page}
        handleNext={handleNext}
        handlePrevius={handlePrevius}
        totalpage={totalpage}
        prevState={previus}
        nextState={next}
      />
    </Box>
  );
}
