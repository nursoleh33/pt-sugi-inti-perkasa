import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination, Navigation } from "swiper";
import "swiper/swiper.min.css";
import "swiper/css/grid";
import "swiper/css/pagination";
import "swiper/css";
import "../App.css";
import { useParams } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";
import { url, publicHeader } from "../assets/helpers/config.jsx";
import {
  Grid,
  Typography,
  Container,
  Box,
  Card,
  CardMedia,
  Skeleton,
  Stack,
  Chip,
} from "@mui/material";
import { makeStyles, styled } from "@mui/styles";
import { red } from "@mui/material/colors";
import Tab from "@material-ui/core/Tab";
import { TabContext, TabList, TabPanel } from "@material-ui/lab";
import SkeletonClients from "../component/Skeleton/Skeleton";
import SkeletonTestimoni from "../component/Skeleton/SkeletonTestimoni";
import Banner from "../component/Banner";
import Similar from "../component/Similar";
import SkeletonBanner from "../component/Skeleton/Banner";
import Clients from "../component/Clients";
import CardTestimoni from "../component/Testimoni";
import Appliction from "../component/Appliction";
import Feature from "../component/Feature";

const useStyles = makeStyles({
  myImage: {
    marginTop: "40px",
    height: "300px",
    width: "100%",
  },
  myDiv: {
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "row",
    gap: "20",
    maxWidth: "100%",
    backgroundColor: "#000",
    marginTop: "120px",
    paddingTop: "10px",
  },
  myThumbnailimg: {
    marginRight: "50px",
    paddingRight: "50px",
    cursor: "pointer",
  },
  myMaintenace: {
    display: "flex",
    justifyContent: "center",
    color: red,
    alignItems: "center",
    marginTop: 180,
  },
  myNotFound: {
    display: "flex",
    justifyContent: "center",
    fontWeight: "bold",
    color: red,
    alignItems: "center",
    marginTop: 150,
  },
  mySemilar: {
    marginTop: 1,
    marginBottom: 30,
  },
  myContent: {
    backgroundColor: "#ECF0FF",
    color: "common.black",
  },
  myTestimoni: {
    display: "flex",
    justifyContent: "flex-start",
    marginTop: 1,
    marginBottom: 5,
    backgroundColor: "#ECF0FF",
    color: "common.black",
  },
  myClients: {
    display: "flex",
    justifyContent: "center",
    marginTop: 1,
    marginBottom: 5,
  },
});

const CustomTypograpy = styled(Typography)(({ theme }) => ({
  fontWeight: "bold",
  color: theme.palette.primary.main,
  textTransform: "uppercase",
  marginLeft: 70,
}));
const ProductsDetails = () => {
  let { id } = useParams();
  const [value, setValue] = useState("1");
  let [semilar, setSemilar] = useState([]);
  let [banner, setBanner] = useState([]);
  let [clients, setClients] = useState(null);
  let [testimoni, setTestimoni] = useState([]);
  let [handle, setHandle] = useState(false);
  let [stockSemilar, setStockSemilar] = useState(false);
  let [stockClient, setStockClient] = useState(false);
  let [stockTestimoni, setStockTestimoni] = useState(false);
  let [loading, setLoading] = useState(false);
  let [detailProducts, setDetailProducts] = useState({
    brand: "",
    application: "",
    overview: "",
    feature: "",
    status: "",
    image_url: "",
    image_url1: "",
    image_url2: "",
    image_url3: "",
    desc: "",
  });
  let [image, setImage] = useState("");
  const HandleChange = (event, newValue) => {
    setValue(newValue);
  };

  const getDataProducts = async () => {
    setLoading(true);
    await axios({
      url: `${url.api}/products/${id}`,
      method: "GET",
      headers: publicHeader(),
      data: {},
    })
      .then((res) => {
        if (res.status === 200) {
          let data = res.data.data;
          setDetailProducts({
            brand: data?.name,
            application: data?.application,
            feature: data?.feature,
            overview: data?.overview,
            status: data?.status,
            image_url: data?.image_url,
            image_url1: data?.image_url1,
            image_url2: data?.image_url2,
            image_url3: data?.image_url3,
            desc: data?.brand?.name,
          });
          setImage(data?.image_url);
        }
        setLoading(false);
      })
      .catch((err) => {
        Swal.fire("Sorry", "Data Produk Gagal Di Tampilkan", "warning");
        setLoading(false);
      });
  };

  const handleAddSemilar = async () => {
    setLoading(true);
    try {
      let post = detailProducts.brand;
      let productSimilar = await axios({
        url: `${url.api}/similar`,
        method: "POST",
        headers: publicHeader(),
        data: post,
      });

      let dataBanner = await axios({
        url: `${url.api}/banners`,
        method: "GET",
        headers: publicHeader(),
        data: {},
      });
      let dataClinet = await axios({
        url: `${url.api}/clients`,
        method: "GET",
        headers: publicHeader(),
        data: {},
      });
      let dataTestimoni = await axios({
        url: `${url.api}/testimoni`,
        method: "GET",
        headers: publicHeader(),
        data: {},
      });
      if (
        productSimilar.status === 200 ||
        dataClinet.status === 200 ||
        dataBanner.status === 200 ||
        dataTestimoni.status === 200
      ) {
        let productSemilar = productSimilar.data.data;
        let banner = dataBanner.data.data;
        let clinet = dataClinet.data.data.data;
        let testimoni = dataTestimoni.data.data;
        setClients(clinet);
        setBanner(banner);
        setSemilar(productSemilar);
        setTestimoni(testimoni);
        setHandle(false);
        setLoading(false);
        setStockClient(clinet.length == null ? 0 : clinet.length);
        setStockTestimoni(testimoni.length == null ? 0 : testimoni.length);
        setStockSemilar(
          productSemilar.length == null ? 0 : productSemilar.length
        );
      }
      if (
        productSimilar.status === 500 ||
        productSimilar.status === 404 ||
        dataClinet.status === 500 ||
        dataClinet.status === 404 ||
        dataBanner.status === 500 ||
        dataBanner.status === 404 ||
        dataTestimoni.status === 500 ||
        dataTestimoni.status === 404
      ) {
        setHandle(true);
        setLoading(true);
      }
    } catch (error) {
      Swal.fire("Sorry", "Data Semilar Gagal  Di Tampilkan", "warning");
      setLoading(true);
    }
  };

  useEffect(() => {
    getDataProducts();
  }, []);
  useEffect(() => {
    handleAddSemilar();
  }, []);
  // useEffect(() => {
  //   // ("clients", clients.length);
  //   ("clinet", clients);
  // }, [clients]);
  const classes = useStyles();
  const handleImage = (value) => {
    setImage(value);
  };
  if (handle) {
    return (
      <h1 className={classes.myMaintenace}>Sedang Dalam Maintenace Server</h1>
    );
  }
  if (
    stockClient === null ||
    stockSemilar === null ||
    stockTestimoni === null
  ) {
    return <h1 className={classes.myNotFound}>Data Tidak Di Temukan</h1>;
  }
  return (
    <Box sx={{ paddingTop: 12 }}>
      <Helmet>
        <title>Product Detail</title>
        <meta name="description" content="Sugi Inti Perkasa" />
      </Helmet>
      {loading ? (
        <SkeletonBanner />
      ) : (
        banner.map((item, index) => {
          return (
            <span key={index}>
              <Banner id={item.id} title={item.title} image={item.image_url} />
            </span>
          );
        })
      )}
      ;
      <Box>
        <Container maxWidth="lg" sx={{ py: 2 }}>
          <Grid container alignItems="center" spacing={3}>
            <Grid item xs={12} lg={4} md={4}>
              <Card
                sx={{ width: 300, height: 250, marginBottom: 5 }}
                raised={true}
              >
                {loading ? (
                  <Skeleton
                    animation="pulse"
                    variant="rounded"
                    height={230}
                    width={260}
                    style={{
                      marginLeft: "auto",
                      marginRight: "auto",
                      marginTop: 8,
                      marginBottom: 8,
                    }}
                  />
                ) : (
                  <CardMedia
                    component="img"
                    height="100%"
                    image={image}
                    alt="product"
                  />
                )}
              </Card>
              <Grid
                container
                alignItems="center"
                justifyContent="space-between"
                gap={2}
                wrap="nowrap"
                sx={{
                  width: 350,
                  marginBottom: {
                    xs: 5,
                    md: 0,
                    lg: 0,
                    // backgroundColor: "red",
                    marginLeft: -20,
                  },
                }}
              >
                <Grid item xs={12} lg={3} md={3}>
                  {loading ? (
                    <Skeleton
                      animation="pulse"
                      variant="rounded"
                      height={100}
                      width={80}
                    />
                  ) : (
                    detailProducts.image_url1 && (
                      <Card raised={true}>
                        <img
                          src={detailProducts.image_url1}
                          alt="image"
                          width="100px"
                          height="100px"
                          style={{ cursor: "pointer", objectFit: "cover" }}
                          onClick={() => handleImage(detailProducts.image_url1)}
                        />
                      </Card>
                    )
                  )}
                </Grid>
                <Grid item xs={12} lg={3} md={3}>
                  {loading ? (
                    <Skeleton
                      animation="pulse"
                      variant="rounded"
                      height={100}
                      width={80}
                    />
                  ) : (
                    detailProducts.image_url2 && (
                      <Card raised={true}>
                        <img
                          src={detailProducts.image_url2}
                          alt="iamge"
                          width="100px"
                          height="100px"
                          style={{ cursor: "pointer", objectFit: "cover" }}
                          onClick={() => handleImage(detailProducts.image_url2)}
                        />
                      </Card>
                    )
                  )}
                </Grid>
                <Grid item xs={12} lg={3} md={3}>
                  {loading ? (
                    <Skeleton
                      animation="pulse"
                      variant="rounded"
                      height={100}
                      width={80}
                    />
                  ) : (
                    detailProducts.image_url3 && (
                      <Card raised={true}>
                        <img
                          src={detailProducts.image_url3}
                          alt="iamge"
                          width="100px"
                          height="100px"
                          style={{ cursor: "pointer", objectFit: "cover" }}
                          onClick={() => handleImage(detailProducts.image_url3)}
                        />
                      </Card>
                    )
                  )}
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} md={8} lg={8}>
              {loading ? (
                <>
                  <Skeleton
                    animation="pulse"
                    height={30}
                    width={100}
                    style={{
                      marginBottom: 5,
                    }}
                    variant="text"
                  />
                  <Skeleton
                    animation="pulse"
                    height={30}
                    width={150}
                    style={{
                      marginBottom: 5,
                    }}
                    variant="text"
                  />
                </>
              ) : (
                <>
                  <Typography
                    variant="h6"
                    align="left"
                    gutterBottom
                    sx={{ fontWeight: "bold" }}
                  >
                    {detailProducts.brand}
                  </Typography>
                  <Typography
                    variant="h6"
                    gutterBottom
                    color="#0B247B"
                    sx={{ textTransform: "uppercase" }}
                  >
                    {detailProducts.desc}
                  </Typography>
                </>
              )}
              <Stack direction="row" rowGap={3}>
                {loading ? (
                  <Skeleton
                    animation="pulse"
                    height={60}
                    width={110}
                    variant="text"
                    sx={{ borderRadius: 6 }}
                  />
                ) : (
                  <Chip
                    label={
                      detailProducts.status >= 1
                        ? "Available Stock"
                        : "Sold Stock"
                    }
                    color={detailProducts.status >= 1 ? "secondary" : "error"}
                  />
                )}
              </Stack>
              <TabContext value={value}>
                <Box
                  sx={{
                    borderBottom: 1,
                    borderColor: "divider",
                    color: "info.main",
                    display: "flex",
                    justifyContent: {
                      xs: "center",
                      md: "flex-start",
                      lg: "flex-start",
                    },
                  }}
                >
                  <TabList onChange={HandleChange} aria-label="Product Tab">
                    <Tab label="Overview" value="1" />
                    <Tab label="Aplications" value="2" />
                  </TabList>
                </Box>
                <TabPanel value="1">
                  <Container maxWidth="md">
                    <Stack direction="row">
                      <Chip
                        label="Overview"
                        size="medium"
                        variant="filled"
                        sx={{
                          width: "100%",
                          borderRadius: "3px",
                          backgroundColor: "#ECECEC",
                        }}
                      />
                    </Stack>
                    <Box
                      sx={{
                        maxWidth: "400px",
                        textTransform: "capitalize",
                        // marginLeft: { xs: 2 },
                        // marginTop: 5,
                      }}
                    >
                      <Typography component="p">
                        {detailProducts.overview}
                      </Typography>
                      <Typography
                        variant="h6"
                        sx={{
                          color: "primary.main",
                          marginTop: 2,
                          fontWeight: "bold",
                        }}
                      >
                        Feature
                      </Typography>
                      <Stack>
                        <Feature desc={detailProducts.feature} />
                      </Stack>
                    </Box>
                  </Container>
                </TabPanel>
                <TabPanel value="2">
                  <Container maxWidth="md">
                    <Stack direction="row">
                      <Chip
                        label="Applications"
                        size="medium"
                        variant="filled"
                        sx={{
                          width: "100%",
                          borderRadius: "3px",
                          backgroundColor: "#ECECEC",
                        }}
                      />
                    </Stack>
                    <Stack>
                      <Appliction desc={detailProducts.application} />
                    </Stack>
                  </Container>
                </TabPanel>
              </TabContext>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box>
        <Typography
          variant="h4"
          component="h2"
          style={{
            textAlign: "left",
            marginBottom: 5,
            marginTop: 15,
            marginLeft: 15,
            fontWeight: "600",
            color: "#0B247B",
          }}
        >
          SIMILAR PRODUCTS
        </Typography>
        <Swiper
          centeredSlides={false}
          pagination={{ clickable: true }}
          modules={[Navigation, Pagination]}
          spaceBetween={1}
          slidesPerView={4}
          navigation
          scrollbar={{ draggable: true }}
          breakpoints={{
            1024: {
              slidesPerView: 4,
              spaceBetween: 40,
              slidesPerColumn: 4,
            },
            768: {
              slidesPerView: 2,
              spaceBetween: 30,
              slidesPerColumn: 2,
            },
            640: {
              slidesPerView: 2,
              spaceBetween: 30,
              slidesPerColumn: 2,
            },
            320: {
              slidesPerView: 1,
              spaceBetween: 10,
              slidesPerColumn: 1,
            },
          }}
        >
          {semilar.map((milar) => {
            return (
              <SwiperSlide className={classes.mySemilar} key={milar.id}>
                <Similar
                  id={milar.id}
                  img={milar.image_url}
                  desc={milar.description}
                  headline={milar.name}
                />
              </SwiperSlide>
            );
          })}
        </Swiper>
      </Box>
      <Box>
        <Typography
          variant="h5"
          style={{
            textAlign: "left",
            marginBottom: 5,
            marginTop: 15,
            marginLeft: 15,
            fontWeight: "600",
            color: "#0B247B",
          }}
        >
          RECOMENDED PRODUCTS
        </Typography>
        <Swiper
          // centeredSlides={true}
          pagination={{ clickable: true }}
          modules={[Navigation, Pagination]}
          spaceBetween={5}
          slidesPerView={1}
          navigation
          scrollbar={{ draggable: true }}
          breakpoints={{
            1024: {
              slidesPerView: 4,
              spaceBetween: 40,
              slidesPerColumn: 4,
            },
            768: {
              slidesPerView: 2,
              spaceBetween: 30,
              slidesPerColumn: 2,
            },
            640: {
              slidesPerView: 2,
              spaceBetween: 30,
              slidesPerColumn: 2,
            },
            320: {
              slidesPerView: 1,
              spaceBetween: 10,
              slidesPerColumn: 1,
            },
          }}
        >
          {semilar.map((milar) => {
            return (
              <SwiperSlide className={classes.mySemilar} key={milar.id}>
                <Similar
                  id={milar.id}
                  img={milar.image_url}
                  desc={milar.description}
                  headline={milar.name}
                />
              </SwiperSlide>
            );
          })}
        </Swiper>
      </Box>
      <Container maxWidth="lg">
        <Typography
          variant="h4"
          align="center"
          sx={{
            marginTop: 7,
            marginBottom: 5,
            color: "#000B31",
            fontWeight: 600,
          }}
        >
          OUR CLIENTS
        </Typography>
        <Grid
          container
          alignItems="start"
          spacing={6}
          className={classes.myClients}
        >
          {loading ? (
            <>
              <SkeletonClients />
            </>
          ) : (
            clients?.map((item) => {
              return (
                <Grid item xs={12} lg={6} md={6} key={item.id}>
                  <Clients
                    image={item.image_url}
                    name={item.name}
                    description={item.description}
                  />
                </Grid>
              );
            })
          )}
        </Grid>
      </Container>
      <Box className={classes.myContent} sx={{ pb: 2 }}>
        <Container maxWidth="lg">
          <Typography
            variant="h4"
            align="center"
            sx={{
              marginTop: 10,
              py: 5,
              color: "#000B31",
              fontWeight: 600,
            }}
          >
            TESTIMONI
          </Typography>
          <Swiper
            spaceBetween={1}
            centeredSlides={true}
            autoplay={{ delay: 3500, disableOnInteraction: true }}
            pagination={{ clickable: true }}
            modules={[Autoplay, Pagination]}
          >
            {loading ? (
              <SkeletonTestimoni />
            ) : (
              testimoni.map((item) => {
                return (
                  <SwiperSlide className={classes.myTestimoni} key={item.id}>
                    <CardTestimoni
                      jabatan={item.jabatan}
                      image={item.image_url}
                      name={item.name}
                      description={item.description}
                      testimoni={item.testimoni}
                    />
                  </SwiperSlide>
                );
              })
            )}
          </Swiper>
        </Container>
      </Box>
    </Box>
  );
};

export default ProductsDetails;
