import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import axios from "axios";
import Swal from "sweetalert2";
import Cards from "../component/Cards";
import { Box, Container, Grid } from "@mui/material";
import { red } from "@mui/material/colors";
import { makeStyles } from "@mui/styles";
import SkeletonService from "../component/Skeleton/SkeletonService";
import CardService from "../component/Service";
import { url, publicHeader } from "../assets/helpers/config.jsx";

const useStyles = makeStyles({
  myMaintenace: {
    display: "flex",
    justifyContent: "center",
    color: red,
    alignItems: "center",
    marginTop: 180,
  },
  myNotFound: {
    display: "flex",
    justifyContent: "center",
    fontWeight: "bold",
    color: red,
    alignItems: "center",
    marginTop: 150,
  },
});
function Service() {
  let [service, setService] = useState([]);
  let [loading, setLoading] = useState(false);
  let [stockService, setStockService] = useState(false);
  let [handle, setHandle] = useState(false);
  const getData = async () => {
    setLoading(true);
    await axios({
      url: `${url.api}/services`,
      method: "GET",
      headers: publicHeader(),
      data: {},
    })
      .then((res) => {
        if (res.status === 200) {
          let data = res.data.data.data;
          setService(data);
          setStockService(!data.length === true ? 0 : data.length);
          setHandle(false);
          setLoading(false);
        }
        if (res.status === 500 || res.status === 404) {
          setHandle(true);
          setLoading(false);
        }
      })
      .catch((err) => {
        Swal.fire("Sorry", "Data Gagal Di Tampilkan", "warning");
        setHandle(true);
        setLoading(false);
      });
  };
  useEffect(() => {
    getData();
  }, []);
  const classes = useStyles();
  if (handle) {
    return (
      <h1 className={classes.myMaintenace}>Sedang Dalam Maintenace Server</h1>
    );
  }
  if (stockService === 0) {
    return <h1 className={classes.myNotFound}>Data Tidak Di Temukan</h1>;
  }
  return (
    <Box sx={{ marginBottom: 10 }}>
      <Helmet>
        <title>Service</title>
        <meta name="description" content="Sugi Inti Perkasa" />
      </Helmet>
      <Cards
        title="SERVICE"
        desc="Looking for an industrial maintenance and services company that provides various machine tools and compressors? Look no further than Sugi Inti Pekasa! Our team of experts provides top-quality services that are sure to meet your needs. Whether you need a new compressor or machine tool, we've got you covered. Contact us today to learn more!"
      />
      <Box
        sx={{
          display: "flex",
          justifyContent: "start",
        }}
      >
        <Container maxWidth="lg">
          <Grid
            container
            alignItems="start"
            // justifyContent="start"
            // rowSpacing={3}
            spacing={1}
            // sx={{ display: "flex", justifyContent: "start" }}
            // rowGap={5}
          >
            {loading ? (
              <>
                <SkeletonService />
              </>
            ) : (
              service.map((servis) => {
                return (
                  <Grid item xs={12} lg={12} md={12} key={servis.id}>
                    <CardService
                      image={servis.image_url}
                      desc={servis.description}
                      title={servis.title}
                    />
                  </Grid>
                );
              })
            )}
          </Grid>
        </Container>
      </Box>
    </Box>
  );
}

export default Service;
