import React, { useEffect, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "../App.css";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Autoplay, Pagination } from "swiper";
import { Helmet } from "react-helmet";
import {
  Grid,
  Typography,
  Container,
  Box,
  List,
  ListItem,
  ListItemText,
  CardMedia,
  ListItemAvatar,
  Button,
} from "@mui/material";
import CardTestimoni from "../component/Testimoni";
import SkeletonClients from "../component/Skeleton/Skeleton";
import ArrowCircleRightIcon from "@mui/icons-material/ArrowCircleRight";
import Swal from "sweetalert2";
import { makeStyles } from "@mui/styles";
import { red } from "@mui/material/colors";
import Products from "../assets/images/product.png";
import Support from "../assets/images/itsupport.png";
import CuttingTools from "../assets/images/cuttingtools.png";
import Automation from "../assets/images/automation.png";
import GeneralConsume from "../assets/images/general_consumable.png";
import Cad from "../assets/images/cadcam.png";
import Industrial from "../assets/images/idustrial.png";
import SugiInti from "../assets/images/sugiinti.png";
import Service from "../assets/images/Service.png";
import Jumbotron from "../component/Hero";
import SugiIntiPerkasa from "../assets/images/sugiintiperkasa.png";
import Satu from "../assets/images/satu.png";
import Dua from "../assets/images/dua.png";
import Tiga from "../assets/images/tiga.png";
import axios from "axios";
import Partnership from "../component/Skeleton/partnership";
import Clients from "../component/Clients";
import SkeletonTestimoni from "../component/Skeleton/SkeletonTestimoni";
import "../App.css";
import { url, publicHeader } from "../assets/helpers/config.jsx";
import Btn from "../component/Btn";

const useStyles = makeStyles({
  myImg: {
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    objectFit: "cover",
    marginBottom: "10px",
    width: "100%",
  },
  myImage: {
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    objectFit: "contain",
    marginBottom: "15px",
  },
  myBanner: {
    backgroundRepeat: "no-repeat",
    objectFit: "cover",
    width: "100%",
  },
  myGrid: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  myProduct: {
    paddingRight: 15,
  },
  myContent: {
    backgroundColor: "#ECF0FF",
    color: "common.black",
  },
  myHeadline: {
    backgroundColor: "#F5F5F5",
    color: "#002F52",
  },
  myPartnership: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 5,
    alignItems: "center",
  },
  myImageParthership: {
    width: "auto",
    height: "auto",
    objectFit: "cover",
    // marginBottom: -4,
  },
  myTestimoni: {
    display: "flex",
    justifyContent: "flex-start",
    marginTop: 1,
    marginBottom: 5,
    backgroundColor: "#ECF0FF",
    color: "common.black",
  },
  myClients: {
    display: "flex",
    justifyContent: "center",
    marginTop: 1,
    marginBottom: 5,
  },
  myMaintenace: {
    display: "flex",
    justifyContent: "center",
    color: red,
    alignItems: "center",
    marginTop: 180,
  },
  myNotFound: {
    display: "flex",
    justifyContent: "center",
    fontWeight: "bold",
    color: red,
    alignItems: "center",
    marginTop: 150,
  },
});
//Membuat Style Reusabled digunakan untuk membuat 1 style component yang dapat digunakan di semua component dari pada buat class setiap masing2 component
// const CustomTypograpy = styled(Typography)(() => ({
//   textAlign: { xs: "center", md: "left" },
// }));

// const Title = ({ text }) => (
//   <Typography
//     variant="h4"
//     component="h1"
//     gutterBottom
//     sx={{
//       textAlign: { xs: "left", md: "left" },
//       width: { xs: "500px", lg: "100%", md: "500px" },
//       marginLeft: "23px",
//     }}
//   >
//     {text}
//   </Typography>
// );
const Description = ({ text, desc }) => (
  <ListItemText
    primary={
      <Typography variant="h6" style={{ color: "#002F52", fontWeight: "bold" }}>
        {text}
      </Typography>
    }
    secondary={
      <Typography variant="p" style={{ color: "#494F66", fontWeight: "200" }}>
        {desc}
      </Typography>
    }
    sx={{ textAlign: "justify" }}
  />
);
function Home() {
  const classes = useStyles();
  let [partnership, setPartnership] = useState([]);
  let [testimoni, setTestimoni] = useState([]);
  let [clients, setClients] = useState([]);
  let [loading, setLoading] = useState(false);
  let [stockTestimoni, setStockTestimoni] = useState(false);
  let [stockClients, setStockClients] = useState(false);
  let [stockPartnership, setStockPartnership] = useState(false);
  let [handle, setHandle] = useState(false);

  const getData = async () => {
    setLoading(true);
    try {
      let client = await axios({
        url: `${url.api}/clients`,
        method: "GET",
        headers: publicHeader(),
        data: {},
      });
      let testimoni = await axios({
        url: `${url.api}/testimoni`,
        method: "GET",
        headers: publicHeader(),
        data: {},
      });
      let partnership = await axios({
        url: `${url.api}/partnerships`,
        method: "GET",
        headers: publicHeader(),
        data: {},
      });
      if (
        client.status === 200 &&
        testimoni.status === 200 &&
        partnership.status === 200
      ) {
        let dataClinet = client.data.data.data;
        let dataTestimoni = testimoni.data.data;
        let datapartnership = partnership.data.data.data;
        setClients(dataClinet);
        setTestimoni(dataTestimoni);
        setPartnership(datapartnership);
        setStockClients(!dataClinet.length === true ? 0 : dataClinet.length);
        setStockTestimoni(
          !dataTestimoni.length === true ? 0 : dataTestimoni.length
        );
        setStockPartnership(
          !datapartnership.length === true ? 0 : datapartnership.length
        );
        setHandle(false);
        setLoading(false);
      }
      if (
        client.status === 404 ||
        (client.status === 500 && testimoni.status === 404) ||
        testimoni.status === 500
      ) {
        setHandle(true);
        setLoading(false);
      }
    } catch (error) {
      Swal.fire("Sorry", "Data Gagal Di Tampilkan", "warning");
      setLoading(false);
    }
  };
  useEffect(() => {
    getData();
  }, []);
  if (handle) {
    return (
      <h1 className={classes.myMaintenace}>Sedang Dalam Maintenace Server</h1>
    );
  }
  // ({ stockClients, stockPartnership, stockTestimoni });
  if (stockClients === 0 || stockPartnership === 0 || stockTestimoni === 0) {
    return <h1 className={classes.myNotFound}>Data Tidak Di Temukan</h1>;
  }
  return (
    <Box>
      <Helmet>
        <title>Home</title>
        <meta name="description" content="Sugi Inti Perkasa" />
      </Helmet>
      <Jumbotron />
      <Box
        sx={{
          backgroundColor: "background.paper",
          color: "common.black",
          // paddingBottom: 20,
          // paddingTop: 20,
        }}
      >
        <Container maxWidth="xl">
          <Grid container alignItems="center" justifyContent="center">
            <Grid
              item
              xs={12}
              lg={7}
              md={7}
              sx={{
                marginLeft: { lg: -5 },
                order: { xs: 1, md: 1 },
                marginTop: { xs: 10 },
                // paddingTop: 10,
                // paddingBottom: 10,
              }}
            >
              <img
                src={Products}
                alt="Products"
                width="100%"
                className={classes.myImg}
              />
            </Grid>
            <Grid
              item
              xs={12}
              lg={5}
              md={5}
              sx={{
                order: { xs: 2, md: 2 },
                marginTop: { xs: 5, md: 15, lg: 15 },
              }}
            >
              <Typography variant="h4" component="h6" gutterBottom>
                We provide various of Industrial Maintenance and Services
                Products.
              </Typography>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={CuttingTools}
                      alt="Cutting Tools"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Cutting Tools"
                    desc="comprehensive cutting tool manufacuturer specialized in the manufacture and sale of taps, drills, endmills, indexable tools and rolling dies."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Automation}
                      alt="Automation"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Automation"
                    desc="We provide system automation services in terms of software and hardware according to your business needs. Our Desktop, mobile, web interface is ready."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={GeneralConsume}
                      alt="GeneralConsume"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="General Consumable"
                    desc="We provide various of Industrial General Consumable and provide Quality | Cost | Delivery | Services manner."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Cad}
                      alt="CAD/CAM Software Solution"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="CAD/CAM Software Solution"
                    desc="Our CAD/CAM software products and automatic nesting applications are improving the productivity of cutting systems, sheet metal machines and welding robots."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Industrial}
                      alt="Industrial AVG Robot Solution"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Industrial AVG Robot Solution"
                    desc="Robotics is a diverse industry, and its future is full of uncertainty: no one can predict where and where it will go in the years ahead."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Support}
                      alt="It Support"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="IT-Support & Solution"
                    desc="We provide various of Industrial ERP System customized base on customer needs."
                  />
                </ListItem>
                <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
                  <Btn />
                </Box>
              </List>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box className={classes.myContent} sx={{ my: 15, py: 10 }}>
        <Container maxWidth="lg">
          <Grid container alignItems="center" columnSpacing={12}>
            <Grid
              item
              xs={12}
              lg={6}
              md={6}
              sx={{ order: { xs: 2, md: 1, lg: 1 }, marginTop: { xs: 5 } }}
            >
              <Typography variant="h4" component="h6" gutterBottom>
                We provide various of Industrial Maintenance and services.
              </Typography>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Service}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Compressor Maintenance & Service"
                    desc="Terima jasa service compressor / service komprsor udara dengan layanan terbaik dan suku cadang berkualitas. Maintenance, repair overhaul dsb."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Service}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Machine Tool Maintenance & Service"
                    desc="Terima jasa service compressor / servis kompresor udara dengan layanan terbaik dan suku cadang berkualitas. Maintenance, repair, overhaul  dsb."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Service}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Panel Box Maintenace & Service"
                    desc="Terima jasa service compressor / service kompresor udara dengan layanan terbaik dan suku cadang berkulitas. Maintenace, repair, overhaul dsb."
                  />
                </ListItem>
              </List>
              <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
                <Btn />
              </Box>
            </Grid>
            <Grid
              item
              xs={12}
              lg={6}
              md={6}
              sx={{ order: { xs: 1, md: 2, lg: 2 } }}
            >
              <img
                src={SugiInti}
                alt="Products"
                width="90%"
                className={classes.myImage}
              />
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box sx={{ pb: 2, mt: 4 }}>
        <Container maxWidth="lg">
          <Grid container alignItems="center" columnSpacing={12}>
            <Grid item xs={12} lg={6} md={6} sx={{ order: { xs: 2, md: 1 } }}>
              <img
                src={SugiIntiPerkasa}
                alt="Products"
                width="100%"
                className={classes.myImg}
                sx={{
                  textAlign: { sx: "center", md: "left" },
                }}
              />
            </Grid>
            <Grid
              item
              xs={12}
              lg={6}
              md={6}
              sx={{ order: { xs: 1, md: 2 }, marginTop: { xs: -5, lg: -10 } }}
            >
              <Typography variant="h4" component="h6" gutterBottom>
                We are committed to giving you the best products that you have
                ever had befoure.
              </Typography>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Satu}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Quality"
                    desc="Each of our products is guaranteed the best quality, for the continuation of the product's useful life."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Dua}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Competitive"
                    desc="Guaranteed competitive prices is one of our priorities for you, guaranteeing you get the best price products."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Tiga}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Support"
                    desc="Not only selling, we guarantee full support for every product you get from us."
                  />
                </ListItem>
              </List>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box className={classes.myHeadline} sx={{ mt: 5, py: 5 }}>
        <Container maxWidth="lg">
          <Typography
            variant="h4"
            align="center"
            sx={{ paddingBottom: 5, color: "#002F52", fontWeight: 600 }}
          >
            PARTNERSHIP
          </Typography>
          <Grid
            container
            alignItems="center"
            justifyContent="center"
            gap={8}
            wrap="wrap"
            className={classes.myPartnership}
          >
            {loading ? (
              <>
                <Partnership />
              </>
            ) : (
              partnership?.map((partner) => {
                return (
                  <Grid item xs={12} lg={3} md={6} key={partner.id}>
                    <CardMedia
                      component="img"
                      className={classes.myImageParthership}
                      image={partner.image_url}
                      alt={partner.name}
                    />
                  </Grid>
                );
              })
            )}
          </Grid>
        </Container>
      </Box>
      <Container maxWidth="lg">
        <Typography
          variant="h4"
          align="center"
          sx={{
            marginTop: 7,
            marginBottom: 5,
            color: "#000B31",
            fontWeight: 600,
          }}
        >
          OUR CLIENTS
        </Typography>
        <Grid
          container
          alignItems="start"
          spacing={6}
          className={classes.myClients}
        >
          {loading ? (
            <>
              <SkeletonClients />
            </>
          ) : (
            clients.map((item) => {
              return (
                <Grid item xs={12} lg={6} md={6} key={item.id}>
                  <Clients
                    image={item.image_url}
                    name={item.name}
                    description={item.description}
                  />
                </Grid>
              );
            })
          )}
        </Grid>
      </Container>
      <Box className={classes.myContent} sx={{ pb: 2 }}>
        <Container maxWidth="lg">
          <Typography
            variant="h4"
            align="center"
            sx={{
              marginTop: 10,
              py: 5,
              color: "#000B31",
              fontWeight: 600,
            }}
          >
            TESTIMONI
          </Typography>
          <Swiper
            spaceBetween={1}
            centeredSlides={true}
            autoplay={{ delay: 3500, disableOnInteraction: true }}
            pagination={{ clickable: true }}
            modules={[Autoplay, Pagination]}
          >
            {loading ? (
              <SkeletonTestimoni />
            ) : (
              testimoni.map((item) => {
                return (
                  <SwiperSlide className={classes.myTestimoni} key={item.id}>
                    <CardTestimoni
                      jabatan={item.jabatan}
                      image={item.image_url}
                      name={item.name}
                      description={item.description}
                      testimoni={item.testimoni}
                    />
                  </SwiperSlide>
                );
              })
            )}
          </Swiper>
        </Container>
      </Box>
    </Box>
  );
}

export default Home;
