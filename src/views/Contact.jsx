import React, { useState } from "react";
import { Helmet } from "react-helmet";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import WhatsAppIcon from "@mui/icons-material/WhatsApp";
import Cards from "../component/Cards";
import { makeStyles } from "@mui/styles";
import { red } from "@mui/material/colors";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { FormControl } from "@mui/material";

const useStyles = makeStyles({
  myInput: {
    color: red,
    " &.MuiInput-colorSecondary": {
      color: "secondary",
    },
  },
  myButton: {
    " &::affter, &::before": {
      backgroundColor: "#000",
      content: "",
      display: "inline-block",
      height: "1px",
      position: "relative",
      verticalAlign: "middle",
      width: "50%",
    },
    " &::before": {
      right: "0.5em",
      marginLeft: "-50%",
    },
  },
  myHeader: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    gap: "3px",
    margin: 33,
  },
  myLineLeft: {
    width: "100%",
    height: "1px",
    backgroundColor: "#AFAFAF",
  },
  myLineRight: {
    width: "100%",
    height: "1px",
    backgroundColor: "#AFAFAF",
  },
});

const Contact = () => {
  const nav = useNavigate();
  const iniState = {
    subject: "",
    messege: "",
  };

  const [initialValues, setInitialValue] = useState(iniState);
  const validationSchema = yup
    .object()
    .shape({
      subject: yup
        .string()
        .required("Subject is required")
        .min(6, "Username must be at least 6 characters"),
      messege: yup
        .string()
        .required("Subject is required")
        .min(6, "Username must be at least 6 characters"),
    })
    .required();
  const {
    register,
    watch,
    reset,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    mode: "onTouched",
    reValidateMode: "onChange",
    defaultValues: initialValues,
    resolver: yupResolver(validationSchema),
  });
  const handleSend = async (data) => {
    window.location.href = `mailto:nursole33@gmail.com?body=${data.messege}`;
  };
  const handleWA = () => {
    window.open(`https://wa.me/message/ZDTUH5VHUUSIC1`, "_blank");
  };
  let isValidForm =
    Object.values(errors).filter((error) => typeof error !== "undefined")
      .length === 0;
  const classes = useStyles();
  return (
    <Box>
      <Helmet>
        <title>Contact</title>
        <meta name="description" content="Sugi Inti Perkasa" />
      </Helmet>
      <Cards
        title="CONTACT US"
        desc="Sugi Inti Pekasa is the best product and services you have never had before! It is committed to giving you the best products with competitive costs that you have never had before. It is guaranteed that their competitive prices are one of their priorities for their customers. They are committed on providing the best price solution for your need."
      />
      <Box sx={{ my: 7 }}>
        <Container maxWidth="lg">
          <Grid container alignItems="start" spacing={3}>
            <Grid item xs={12} md={12} lg={12}>
              <form onSubmit={handleSubmit(handleSend)}>
                <FormControl fullWidth>
                  <Box sx={{ mx: 4, my: 2 }}>
                    <TextField
                      fullWidth
                      variant="filled"
                      label="Subject"
                      type="text"
                      placeholder="Masukan Subject Email Anda"
                      autoFocus={true}
                      color="primary"
                      size="medium"
                      {...register("subject")}
                      error={errors.subject ? true : false}
                      // helperText={errors.subject}
                      sx={{ bgcolor: "common.white" }}
                    />
                    <Typography
                      variant="inherit"
                      color="#fc1303"
                      gutterBottom={true}
                    >
                      {errors.subject?.message}
                    </Typography>
                  </Box>
                  <Box sx={{ mx: 4, my: 2 }}>
                    <TextField
                      fullWidth
                      variant="filled"
                      label="Your Message"
                      placeholder="Masukan Message Anda"
                      {...register("messege")}
                      error={errors.messege ? true : false}
                      // helperText={errors.messege}
                      multiline
                      rows={6}
                      type="email"
                      sx={{ bgcolor: "common.white" }}
                    />
                    <Typography
                      variant="inherit"
                      color="#fc1303"
                      gutterBottom={true}
                    >
                      {errors.messege?.message}
                    </Typography>
                  </Box>
                  <Box sx={{ mx: 4, my: 2 }}>
                    <Button
                      type="submit"
                      disabled={!isValidForm}
                      variant="contained"
                      size="large"
                      color="primary"
                      fullWidth
                      sx={{ fontWeight: "bold" }}
                    >
                      Send Message
                    </Button>
                  </Box>
                  <Box className={classes.myHeader}>
                    <div className={classes.myLineLeft}></div>
                    <Typography
                      variant="h6"
                      component="h6"
                      align="center"
                      className={classes.myButton}
                    >
                      OR
                    </Typography>
                    <div className={classes.myLineRight}></div>
                  </Box>
                </FormControl>
                <Box sx={{ mx: 4, my: 2 }}>
                  <Button
                    variant="contained"
                    // disabled={!isValidForm}
                    size="medium"
                    color="secondary"
                    fullWidth
                    sx={{ fontWeight: "bold", color: "#FFFFFF" }}
                    onClick={handleWA}
                  >
                    <WhatsAppIcon
                      color="#FFF"
                      fontSize="medium"
                      // sx={{ paddingRight: 1 }}
                    />
                    WhatsApp
                  </Button>
                </Box>
              </form>
            </Grid>
          </Grid>
        </Container>
      </Box>
    </Box>
  );
};

export default Contact;
