import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import Swal from "sweetalert2";
import SkeletonBanner from "../component/Skeleton/Banner";
import Partnership from "../component/Skeleton/partnership";
import { url, publicHeader } from "../assets/helpers/config.jsx";
import Clients from "../component/Clients";
import Banner from "../component/Banner";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination } from "swiper";
import {
  Box,
  Typography,
  Container,
  Breadcrumbs,
  Link,
  Grid,
  CardMedia,
} from "@mui/material";
import { makeStyles, styled } from "@mui/styles";
import { grey, red } from "@mui/material/colors";
import axios from "axios";
import SkeletonKategori from "../component/Skeleton/SkeletonCategory";
import CardTestimoni from "../component/Testimoni";
import SkeletonClients from "../component/Skeleton/Skeleton";
import SkeletonTestimoni from "../component/Skeleton/SkeletonTestimoni";
import KategoryProduct from "../component/KategoryProduct";

const useStyles = makeStyles({
  myLink: {
    color: red[900],
    fontWeight: "bold",
    textDecoration: "none",
  },
  myData: {
    textDecoration: "none",
    color: grey[900],
  },
  myMaintenace: {
    display: "flex",
    justifyContent: "center",
    color: red,
    alignItems: "center",
    marginTop: 180,
  },
  myNotFound: {
    display: "flex",
    justifyContent: "center",
    fontWeight: "bold",
    color: red,
    alignItems: "center",
    marginTop: 150,
  },
  myTestimoni: {
    display: "flex",
    justifyContent: "flex-start",
    marginTop: 1,
    marginBottom: 5,
    backgroundColor: "#ECF0FF",
    color: "common.black",
  },
  myPartnership: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 5,
    alignItems: "center",
  },
  myContent: {
    backgroundColor: "#ECF0FF",
    color: "common.black",
  },
});
const Products = () => {
  let [products, setProducts] = useState([]);
  let [testimoni, setTestimoni] = useState([]);
  let [partnership, setPartnership] = useState([]);
  let [clients, setClients] = useState([]);
  let [handle, setHandle] = useState(false);
  let [banner, setBanner] = useState([]);
  let [stockproducts, setStockproducts] = useState(false);
  let [stockPartnership, setStockPartnership] = useState(false);
  let [stocktestimoni, setStocktestimoni] = useState(false);
  let [stockclient, setStockClient] = useState(false);
  const [loading, setLoading] = useState(false);
  const handelData = async () => {
    try {
      setLoading(true);
      let dataProducts = await axios({
        url: `${url.api}/kategoris`,
        method: "GET",
        headers: publicHeader(),
      });
      setLoading(true);
      let dataClient = await axios({
        url: `${url.api}/clients`,
        method: "GET",
        headers: publicHeader(),
      });
      setLoading(true);
      let dataBanner = await axios({
        url: `${url.api}/banners`,
        method: "GET",
        headers: publicHeader(),
      });
      let dataTestimoni = await axios({
        url: `${url.api}/testimoni`,
        method: "GET",
        headers: publicHeader(),
      });
      let dataPartnership = await axios({
        url: `${url.api}/partnerships`,
        method: "GET",
        headers: publicHeader(),
        data: {},
      });
      if (
        dataProducts.status === 200 &&
        dataClient.status === 200 &&
        dataBanner.status === 200 &&
        dataTestimoni.status === 200 &&
        dataPartnership.status === 200
      ) {
        let products = dataProducts.data.data.data;
        let client = dataClient.data.data.data;
        let banner = dataBanner.data.data;
        let testimoni = dataTestimoni.data.data;
        let partnership = dataPartnership.data.data.data;
        setProducts(products);
        setPartnership(partnership);
        setClients(client);
        setBanner(banner);
        setTestimoni(testimoni);
        setLoading(false);
        setHandle(false);
        setStockPartnership(!partnership.length === true ? 0 : products.length);
        setStockproducts(!products.length === true ? 0 : products.length);
        setStockClient(!client.length === true ? 0 : client.length);
        setStocktestimoni(!testimoni.length === true ? 0 : testimoni.length);
      }
      if (
        dataProducts.status === 500 ||
        dataProducts.status === 404 ||
        dataClient.status === 500 ||
        dataClient.status === 404 ||
        dataBanner.status === 500 ||
        dataBanner.status === 404 ||
        dataTestimoni.status === 500 ||
        dataTestimoni.status === 404 ||
        dataPartnership.status === 500 ||
        dataPartnership.status === 404
      ) {
        setHandle(true);
        setLoading(false);
      }
      // setLoading(false);
    } catch (error) {
      Swal.fire("Sorry", "Data Gagal Di Tampilkan", "warning");
      setLoading(false);
      setHandle(true);
    }
  };
  useEffect(() => {
    handelData();
  }, []);

  const classes = useStyles();
  const limitProducts = products.filter((product) => {
    return parseInt(product.id) <= 8;
  });
  if (handle) {
    return (
      <h1 className={classes.myMaintenace}>Sedang Dalam Maintenace Server</h1>
    );
  }
  if (
    stockproducts === 0 ||
    stocktestimoni === 0 ||
    stockclient === 0 ||
    stockPartnership === 0
  ) {
    return <h1 className={classes.myNotFound}>Data Tidak Di Temukan</h1>;
  }
  return (
    <Box>
      <Helmet>
        <title>Kategori Product</title>
        <meta name="description" content="Sugi Inti Perkasa" />
      </Helmet>
      <Container maxWidth="lg" sx={{ paddingTop: "110px" }}>
        <Typography
          variant="h5"
          align="center"
          sx={{
            marginTop: 2,
            marginBottom: 2,
            color: "#000B31",
            fontWeight: 600,
          }}
        >
          PRUDUCTS SOLUTIONS
        </Typography>
      </Container>
      {loading ? (
        <SkeletonBanner />
      ) : (
        banner.map((item) => {
          return (
            <span key={item.id}>
              <Banner title={item.title} image={item.image_url} />
            </span>
          );
        })
      )}

      <Box>
        <Container maxWidth="lg">
          <Breadcrumbs aria-label="Product">
            <Link underline="hover" to="/" className={classes.myData}>
              Home
            </Link>
            <Link underline="hover" to="/product" className={classes.myLink}>
              Product
            </Link>
          </Breadcrumbs>
          <Grid
            container
            alignItems="center"
            direction="row"
            justifyContent={{
              xs: "center",
              md: "space-between",
              lg: "space-between",
            }}
            spacing={4}
          >
            {loading ? (
              <>
                <SkeletonKategori />
              </>
            ) : (
              limitProducts.map((item) => {
                return (
                  <span key={item.id}>
                    <Grid item xs="auto" lg="auto" md="auto">
                      <KategoryProduct
                        id={item.id}
                        image={item.image_url}
                        name={item.name}
                      />
                    </Grid>
                  </span>
                );
              })
            )}
          </Grid>
        </Container>
      </Box>
      <Box className={classes.myHeadline} sx={{ mt: 5, py: 5 }}>
        <Container maxWidth="lg">
          <Typography
            variant="h4"
            align="center"
            sx={{ paddingBottom: 5, color: "#002F52", fontWeight: 600 }}
          >
            PARTNERSHIP
          </Typography>
          <Grid
            container
            alignItems="center"
            justifyContent="center"
            gap={8}
            wrap="wrap"
            className={classes.myPartnership}
          >
            {loading ? (
              <>
                <Partnership />
              </>
            ) : (
              partnership?.map((partner) => {
                return (
                  <Grid item xs={12} lg={3} md={6} key={partner.id}>
                    <CardMedia
                      component="img"
                      className={classes.myImageParthership}
                      image={partner.image_url}
                      alt={partner.name}
                    />
                  </Grid>
                );
              })
            )}
          </Grid>
        </Container>
      </Box>
      <Container maxWidth="lg">
        <Typography
          variant="h4"
          align="center"
          sx={{
            marginTop: 10,
            marginBottom: 5,
            color: "#000B31",
            fontWeight: 600,
          }}
        >
          OUR CLIENTS
        </Typography>
        <Grid
          container
          alignItems="start"
          spacing={6}
          className={classes.myClients}
        >
          {loading ? (
            <>
              <SkeletonClients />
            </>
          ) : (
            clients.map((item) => {
              return (
                <Grid item xs={12} lg={6} md={6} key={item.id}>
                  <Clients
                    image={item.image_url}
                    name={item.name}
                    description={item.description}
                  />
                </Grid>
              );
            })
          )}
        </Grid>
      </Container>
      <Box className={classes.myContent} sx={{ pb: 2 }}>
        <Container maxWidth="lg">
          <Typography
            variant="h4"
            align="center"
            sx={{
              marginTop: 10,
              py: 5,
              color: "#000B31",
              fontWeight: 600,
            }}
          >
            TESTIMONI
          </Typography>
          <Swiper
            spaceBetween={1}
            centeredSlides={true}
            autoplay={{ delay: 3500, disableOnInteraction: true }}
            pagination={{ clickable: true }}
            modules={[Autoplay, Pagination]}
          >
            {loading ? (
              <SkeletonTestimoni />
            ) : (
              testimoni.map((item) => {
                return (
                  <SwiperSlide className={classes.myTestimoni} key={item.id}>
                    <CardTestimoni
                      jabatan={item.jabatan}
                      image={item.image_url}
                      name={item.name}
                      description={item.description}
                      testimoni={item.testimoni}
                    />
                  </SwiperSlide>
                );
              })
            )}
          </Swiper>
        </Container>
      </Box>
    </Box>
  );
};
export default Products;
