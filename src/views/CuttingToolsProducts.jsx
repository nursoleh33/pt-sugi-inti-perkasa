import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import { useParams, Link } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination } from "swiper";
import { Box, Grid, Container, Typography, Breadcrumbs } from "@mui/material";
import Clients from "../component/Clients";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import SkeletonClients from "../component/Skeleton/Skeleton";
import { makeStyles, styled } from "@mui/styles";
import { blue, grey } from "@mui/material/colors";
import Stack from "@mui/material/Stack";
import CardTestimoni from "../component/Testimoni";
import SkeletonTestimoni from "../component/Skeleton/SkeletonTestimoni";
import Banner from "../component/Banner";
import CardsProducts from "../component/CardsProducts";
import Page from "../component/Page";
import { red } from "@mui/material/colors";
import SkeletonProducts from "../component/Skeleton/SkeletonProducts";
import SkeletonBanner from "../component/Skeleton/Banner";
import { url, publicHeader } from "../assets/helpers/config.jsx";

const useStyles = makeStyles({
  myLink: {
    color: blue[900],
    fontWeight: "bold",
    textDecoration: "none",
  },
  myText: {
    textDecoration: "none",
    color: grey[900],
  },
  myContent: {
    backgroundColor: "#ECF0FF",
    color: "common.black",
  },
  myBanner: {
    backgroundRepeat: "no-repeat",
    objectFit: "cover",
    width: "100%",
    marginTop: 10,
    marginBottom: 20,
  },
  myTestimoni: {
    display: "flex",
    justifyContent: "flex-start",
    marginTop: 1,
    marginBottom: 5,
    backgroundColor: "#ECF0FF",
    color: "common.black",
  },
  myMaintenace: {
    display: "flex",
    justifyContent: "center",
    color: red,
    alignItems: "center",
    marginTop: 180,
  },
  myNotFound: {
    display: "flex",
    justifyContent: "center",
    fontWeight: "bold",
    color: red,
    alignItems: "center",
    marginTop: 150,
  },
  myBox: {
    padding: "20px 0",
    color: "#FFF",
    width: "100%",
    background: `linear-gradient(90deg, #0237F4 -0.87%, rgba(2, 55, 244, 0) 100%)`,
    margin: "30px 0",
  },
  myClients: {
    display: "flex",
    justifyContent: "center",
    marginTop: 1,
    marginBottom: 5,
  },
});
function CuttingToolsProducts() {
  let [products, setProducts] = useState([]);
  let [banner, setBanner] = useState([]);
  let [totalpage, setTotalPage] = useState(null);
  let [clients, setClients] = useState([]);
  let [testimoni, setTestimoni] = useState([]);
  let [kategori, setKategori] = useState(null);
  let [stockProduct, setStockProduct] = useState(false);
  let [stockClients, setStockClients] = useState(false);
  let [stockTestimoni, setStockTestimoni] = useState(false);
  let [handle, setHandle] = useState(false);
  const [loading, setLoading] = useState(false);
  let [page, setPage] = useState(null);
  let [previus, setPrevius] = useState(null);
  let [next, setNext] = useState(null);
  const classes = useStyles();

  const { id } = useParams();
  const getData = async () => {
    try {
      setLoading(true);
      let dataclients = await axios({
        url: `${url.api}/clients`,
        method: "GET",
        headers: publicHeader(),
        data: {},
      });
      setLoading(true);
      let dataBanner = await axios({
        url: `${url.api}/banners`,
        method: "GET",
        headers: publicHeader(),
        data: {},
      });
      setLoading(true);
      let dataTestimoni = await axios({
        url: `${url.api}/testimoni`,
        method: "GET",
        headers: publicHeader(),
        data: {},
      });
      if (
        dataclients.status === 200 &&
        dataBanner.status === 200 &&
        dataTestimoni.status === 200
      ) {
        let client = dataclients.data.data.data;
        let banner = dataBanner.data.data;
        let testimoni = dataTestimoni.data.data;
        setBanner(banner);
        setClients(client);
        setTestimoni(testimoni);
        setStockClients(!client.length === true ? 0 : client.length);
        setStockTestimoni(!testimoni.length === true ? 0 : testimoni.length);
        setHandle(false);
        setLoading(false);
      }
      if (
        dataclients.status === 500 ||
        dataclients.status === 404 ||
        dataTestimoni.status === 500 ||
        dataTestimoni.status === 404
      ) {
        setHandle(true);
        setLoading(false);
      }
    } catch (error) {
      Swal.fire("Sorry", "Data Gagal Di Tampilkan", "warning");
      setHandle(true);
      setLoading(false);
    }
  };
  const getDataCategory = async () => {
    setLoading(true);
    // let data = []
    await axios({
      url: `${url.api}/productbycategory?page=${page}`,
      method: "POST",
      headers: publicHeader(),
      data: { id: id },
    })
      .then((res) => {
        if (res.status === 200) {
          let data = res.data.data;
          setProducts(data.product.data);
          setKategori(data.category);
          setPrevius(data.product.prev_page_url ? true : false);
          setNext(data.product.next_page_url ? true : false);
          setPage(
            products.length < 12 ? 1 : res?.data?.data?.product?.current_page
          );
          setTotalPage(res?.data?.data?.product?.total);
          setLoading(false);
          setHandle(false);
          setStockProduct(
            !data.product.data.length == true ? 0 : data.product.data.length
          );
        }
        if (res.status === 500 || res.status === 404) {
          setHandle(true);
          setLoading(false);
        }
      })
      .catch((err) => {
        setLoading(false);
        Swal.fire("Sorry", `Data Gagal Di Tampilkan`, "error");
      });
  };
  const handleNext = () => {
    if (totalpage !== null) {
      setPage(page + 1);
      // getData();
    }
  };
  const handlePrevious = () => {
    if (page > 1) {
      setPage(page - 1);
      // getData();
    }
  };
  useEffect(() => {
    // getData();
    getDataCategory();
  }, [page]);
  useEffect(() => {
    getData();
  }, []);
  if (handle) {
    return (
      <h1 className={classes.myMaintenace}>Sedang Dalam Maintenace Server</h1>
    );
  }
  if (stockClients === 0 || stockProduct === 0 || stockTestimoni === 0) {
    return <h1 className={classes.myNotFound}>Data Tidak Di Temukan</h1>;
  }
  return (
    <Box sx={{ color: "#383838" }}>
      <Helmet>
        <title>Product</title>
        <meta name="description" content="Nested component" />
      </Helmet>
      <Container
        maxWidth="lg"
        sx={{
          paddingTop: "110px",
          textTransform: "uppercase",
        }}
      >
        <Typography
          variant="h5"
          align="center"
          sx={{
            marginTop: 2,
            marginBottom: 2,
            color: "#000B31",
            fontWeight: 600,
          }}
        >
          Cutting Tools Poducts
        </Typography>
      </Container>
      {loading ? (
        <SkeletonBanner />
      ) : (
        banner.map((item) => {
          return (
            <span key={item.id}>
              <Banner id={item.id} title={item.title} image={item.image_url} />
            </span>
          );
        })
      )}
      <Box>
        <Container maxWidth="lg">
          <Stack spacing={2}>
            <Breadcrumbs
              aria-label="Tools Proucts"
              separator={<ArrowForwardIosIcon fontSize="small" />}
            >
              <Link underline="hover" to="/home" className={classes.myText}>
                Home
              </Link>
              <Link underline="hover" to="/product" className={classes.myText}>
                Products
              </Link>
              <Link
                underline="hover"
                to="/cuttingtools"
                className={classes.myLink}
              >
                {kategori}
              </Link>
            </Breadcrumbs>
          </Stack>
        </Container>
        <Box className={classes.myBox}>
          <Container maxWidth="lg">
            <Typography variant="h5" align="left" sx={{ fontStyle: "oblique" }}>
              Solid | Indexable | Tapping & Reaming | Special Knife Tools
            </Typography>
          </Container>
        </Box>
        <Page
          page={page}
          handleNext={handleNext}
          handlePrevius={handlePrevious}
          totalpage={totalpage}
          prevState={previus}
          nextState={next}
        />
        <Container maxWidth="lg">
          <Grid
            container
            alignItems="center"
            spacing={6}
            sx={{
              display: "flex",
              justifyContent: "center",
              marginBottom: 5,
              marginTop: 7,
            }}
          >
            {loading ? (
              <>
                <SkeletonProducts />
              </>
            ) : (
              products?.map((product) => {
                return (
                  <Grid item xs="auto" lg={3} md="auto" key={product.id}>
                    <CardsProducts
                      id={product.id}
                      img={product.image_url}
                      desc={product.name}
                      headline={product.brand.name}
                      name={kategori}
                    />
                  </Grid>
                );
              })
            )}
          </Grid>
        </Container>
      </Box>
      <Container maxWidth="lg">
        <Typography
          variant="h4"
          align="center"
          sx={{
            marginTop: 7,
            marginBottom: 5,
            color: "#000B31",
            fontWeight: 600,
          }}
        >
          OUR CLIENTS
        </Typography>
        <Grid
          container
          alignItems="start"
          spacing={6}
          className={classes.myClients}
        >
          {loading ? (
            <>
              <SkeletonClients />
            </>
          ) : (
            clients.map((item) => {
              return (
                <Grid item xs={12} lg={6} md={6} key={item.id}>
                  <Clients
                    image={item.image_url}
                    name={item.name}
                    description={item.description}
                  />
                </Grid>
              );
            })
          )}
        </Grid>
      </Container>
      <Box className={classes.myContent} sx={{ pb: 2 }}>
        <Container maxWidth="lg">
          <Typography
            variant="h4"
            align="center"
            sx={{
              marginTop: 10,
              py: 5,
              color: "#000B31",
              fontWeight: 600,
            }}
          >
            TESTIMONI
          </Typography>
          <Swiper
            spaceBetween={1}
            centeredSlides={true}
            autoplay={{ delay: 3500, disableOnInteraction: true }}
            pagination={{ clickable: true }}
            modules={[Autoplay, Pagination]}
          >
            {loading ? (
              <SkeletonTestimoni />
            ) : (
              testimoni.map((item) => {
                return (
                  <SwiperSlide className={classes.myTestimoni} key={item.id}>
                    <CardTestimoni
                      jabatan={item.jabatan}
                      image={item.image_url}
                      name={item.name}
                      description={item.description}
                      testimoni={item.testimoni}
                    />
                  </SwiperSlide>
                );
              })
            )}
          </Swiper>
        </Container>
      </Box>
    </Box>
  );
}

export default CuttingToolsProducts;
