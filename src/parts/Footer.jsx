import React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { makeStyles } from "@mui/styles";
import Logo from "../assets/images/logo.png";
import Stack from "@mui/material/Stack";
import Toolbar from "@mui/material/Toolbar";
import { styled } from "@mui/material/styles";
import { grey } from "@mui/material/colors";
import { BsFacebook, BsTwitter, BsLinkedin, BsYoutube } from "react-icons/bs";
const useStyles = makeStyles({
  myHeadline: {
    color: "#fff",
    fontWeight: "bold",
  },
  myBox: {
    backgroundColor: "common.black",
  },
  myText: {
    color: "#FFF",
    textDecoration: "none",
    paddingRight: 12,
    fontWeight: "lighter",
    " &:hover": {
      fontWeight: "bold",
    },
  },
});
const CustomTypograpy = styled(Typography)(({ theme }) => ({
  fontWeight: "bold",
  color: theme.palette.common.white,
}));
const Footer = () => {
  const classes = useStyles();
  let iconStyles = {
    color: "#fff",
    fontSize: "1.5em",
    paddingRight: 10,
    // paddingTop: 20,
  };
  return (
    <Box>
      <Box
        sx={{
          color: "#FFFFFF",
          display: "flex",
          justifyContent: "space-between",
          backgroundColor: "#030A44",
          paddingTop: 5,
        }}
      >
        <Container maxWidth="lg">
          <Grid container alignItems="center" columnGap={2}>
            <Grid item xs="auto" md="auto" lg="auto">
              <img
                src={Logo}
                alt="PT SUGI INTI PERKASA"
                width="50px"
                height="50px"
              />
            </Grid>
            <Grid item xs="auto" md="auto" lg="auto">
              <Typography
                component="p"
                variant="h5"
                align="center"
                className={classes.myHeadline}
              >
                PT. SUGI INTI PERKASA
              </Typography>
            </Grid>
          </Grid>
          <Grid
            container
            rowGap={5}
            alignItems="start"
            justifyContent="center"
            sx={{ py: 2, display: "flex", flexWrap: "wrap" }}
          >
            <Grid item xs={6} lg={4} md={4}>
              <Stack spacing={2}>
                <CustomTypograpy variant="h6">Products</CustomTypograpy>
                <Typography variant="p">Cutting Tools</Typography>
                <Typography variant="p">Automation</Typography>
                <Typography variant="p">General Consumable</Typography>
                <Typography variant="p">CAD/CAM Software</Typography>
                <Typography variant="p">Industrial AVG Robot</Typography>
                <Typography variant="p">IT Support Solution</Typography>
              </Stack>
            </Grid>
            <Grid item xs={6} lg={4} md={4}>
              <Stack spacing={2}>
                <CustomTypograpy variant="h6">Service</CustomTypograpy>
                <Typography variant="p">Compressor Service</Typography>
                <Typography variant="p">Compressor Maintenance</Typography>
                <Typography variant="p">Machine Tools Service</Typography>
                <Typography variant="p">Machine Tools Maintenance</Typography>
                <Typography variant="p">Panel Box Service</Typography>
              </Stack>
            </Grid>
            <Grid item xs={12} md={4} lg={4}>
              <Stack spacing={1}>
                <CustomTypograpy variant="h6">Office</CustomTypograpy>
                <Typography variant="p">PT SUGI INTI PERKASA</Typography>
                <Typography variant="body1">
                  Jl. MH Thamrin Ruko Roxy Blok B No. 60 Cibatu Lippo Cikarang
                  Bekasi 17530 Indonesia
                </Typography>
              </Stack>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box sx={{ backgroundColor: "common.black", color: "common.white" }}>
        <Container maxWidth="lg" sx={{ py: 3 }}>
          <Grid
            container
            alignItems="start"
            justifyContent="space-between"
            spacing={5}
            sx={{ display: "flex", flexWrap: "wrap" }}
          >
            <Grid item xs={12} md="auto" lg="auto">
              <Typography variant="p">
                COPYRIGHT &copy; 2019-2022 PT SUGI INTI PERKASA. ALL RIGHTS
                RESERVED.
              </Typography>
            </Grid>
            <Grid item xs={12} md="auto" lg="auto">
              <Typography variant="p">
                <BsFacebook style={iconStyles} />
                <BsTwitter style={iconStyles} />
                <BsLinkedin style={iconStyles} />
                <BsYoutube style={iconStyles} />
              </Typography>
            </Grid>
          </Grid>
        </Container>
      </Box>
    </Box>
  );
};
export default Footer;
