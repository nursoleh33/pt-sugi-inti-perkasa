import { Routes, Route } from "react-router-dom";
import "./App.css";
import Home from "./views/Home";
import Products from "./views/Products";
import Service from "./views/Service";
import AboutUs from "./views/About";
import Contact from "./views/Contact";
import CuttingToolsProducts from "./views/CuttingToolsProducts";
import CardsProducts from "./component/CardsProducts";
import ProductsDetails from "./views/ProductsDetails";
import Navbar from "./component/Navbar";
import Footer from "./parts/Footer";
import Catalog from "./views/Catalog";

function App() {
  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/product" element={<Products />} />
        <Route path="/service" element={<Service />} />
        <Route path="/about" element={<AboutUs />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/cuttingtools" element={<CuttingToolsProducts />} />
        <Route path="/cards" element={<CardsProducts />} />
        <Route path="/catalogue" element={<Catalog />} />
        <Route path="/productdetail/:id" element={<ProductsDetails />} />
        <Route path="/categori/:id" element={<CuttingToolsProducts />} />
      </Routes>
      <Footer />
    </>
  );
}

export default App;
